using TerrainGenerator;
using TerrainGenerator.Core;

public class GameState : UnitySingleton<GameState>
{
	public GameManager GameManager { get; set; }

	public InputState InputState { get; set; }

	public IPerformance Performance { get; set; }

	public TerrainState TerrainState { get; set; }

	protected override void OnInitialize()
	{
	}

	private void OnDisable()
	{
		Performance?.Dispose();
	}
}
