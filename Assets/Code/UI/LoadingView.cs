using System;
using System.Collections;
using System.Diagnostics;
using System.Threading.Tasks;
using TerrainGenerator;
using TerrainGenerator.Contracts;
using TerrainGenerator.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;

public class LoadingView : View
{
	[SerializeField]
	private Text contextTitle = null;
	private GameState gameState;
	[SerializeField]
	private View gameView = null;
	private LoadingState loadingState;
	[SerializeField]
	private Slider progressBar = null;
	private TerrainState terrainState;

	private void CreateGenerator()
	{
		var provider = loadingState.SelectedTileProvider;
		var tileProvider = Instantiate(provider, terrainState.TerrainObject.transform, false);
		tileProvider.gameObject.name = provider.name;
		var runner = terrainState.TerrainObject.AddComponent<UnityTaskSchedulerRunner>();
		runner.TaskScheduler = (IExecutePendingTasks)terrainState.GeneratorBackgroundTaskScheduler;
	}

	private void GameTransition()
	{
		ViewStack.Replace(gameView);

		GameState.Active.GameManager.Run();
	}

	private IEnumerator LoadScene()
	{
		var sceneLoadOperation = SceneManager.LoadSceneAsync("Sample", LoadSceneMode.Additive);
		sceneLoadOperation.allowSceneActivation = false;

		while (!sceneLoadOperation.isDone)
		{
			progressBar.value = sceneLoadOperation.progress * 100;
			if (sceneLoadOperation.progress >= 0.9f)
			{
				sceneLoadOperation.allowSceneActivation = true;
				break;
			}
			yield return null;
		}
		progressBar.value = 100;
		yield return sceneLoadOperation;
		var scene = SceneManager.GetSceneByName("Sample");
		SceneManager.SetActiveScene(scene);
		terrainState = TerrainState.Active;
		gameState = GameState.Active;
	}

	private IEnumerator LoadSurround()
	{
		var waiter = new WaitForSeconds(0.2f);

		int length = 3 * 3;
		contextTitle.text = "Loading Environment";
		for (int i = length - 1; i >= 0; i--)
		{
			int x = i % 3 - 1;
			int y = i / 3 % 3 - 1;
			progressBar.value = 100 - (i * 100 / (length - 1));
			var task = terrainState.TerrainController.LoadChunk(new Vector2Int(x, y));
			while (!task.IsCompleted)
			{
				yield return null;
			}
			if (task.IsFaulted)
			{
				Debug.Log(task.Exception);
			}
			//yield return chunk.Render();
			yield return waiter;
		}

		yield return new WaitForSeconds(1);
	}

	private void OnDisable()
	{
		loadingState.Dispose();
		loadingState = null;
	}

	private void OnEnable()
	{
		loadingState = LoadingState.Instance;
	}

	private void PopulateData()
	{
		terrainState.Size = new RectInt()
		{
			min = loadingState.SelectedSize * -1,
			max = loadingState.SelectedSize
		};
		terrainState.SizeFactor = loadingState.SelectedSizeFactor;
		terrainState.BiomeGraph.BiomeSet = loadingState.BiomeSet;

		var performance = AppState.State.GetPerformance(loadingState.SelectedTileProvider.name, loadingState.SelectedSizeName);
		GameState.Active.Performance = performance;
		terrainState.Measure = loadingState.MeasurePerformance ? new PerformanceMeasure(performance) : (IMeasure)new DisabledMeasure();

		terrainState.MainThreadTaskScheduler = AsyncTools.MainThreadScheduler;
		terrainState.PhysicsTaskScheduler = AsyncTools.PhysicsScheduler;
		terrainState.GeneratorBackgroundTaskScheduler = new UnitySingleTaskScheduler(AsyncTools.MainThreadContext, 1);

		var cancelToken = terrainState.CancellationToken;
		terrainState.TaskFactory = new TaskFactory(cancelToken, TaskCreationOptions.DenyChildAttach, TaskContinuationOptions.None, TaskScheduler.Default);
		terrainState.MainThreadTaskFactory = new TaskFactory(cancelToken, TaskCreationOptions.DenyChildAttach, TaskContinuationOptions.None, terrainState.MainThreadTaskScheduler);
		terrainState.PhysicsTaskFactory = new TaskFactory(cancelToken, TaskCreationOptions.DenyChildAttach, TaskContinuationOptions.None, terrainState.PhysicsTaskScheduler);
		terrainState.GeneratorBackgroundTaskFactory = new TaskFactory(cancelToken, TaskCreationOptions.DenyChildAttach, TaskContinuationOptions.None, terrainState.GeneratorBackgroundTaskScheduler);
	}

	private IEnumerator PreloadChunks()
	{
		var length = (terrainState.Size.width + 1) * (terrainState.Size.height + 1);
		contextTitle.text = "Pre Loading Chunks.";
		for (int i = length - 1; i >= 0; i--)
		{
			int x = i % (terrainState.Size.width + 1) + terrainState.Size.min.x;
			int y = i / (terrainState.Size.width + 1) % (terrainState.Size.height + 1) + terrainState.Size.min.y;

			progressBar.value = 100 - (i * 100 / (length - 1));
			var task = terrainState.TerrainController.LoadChunk(new Vector2Int(x, y));
			while (!task.IsCompleted)
			{
				yield return null;
			}
			if (task.IsFaulted)
			{
				Debug.LogException(task.Exception);
			}

			yield return null;
		}
	}

	private IEnumerator Start()
	{
		yield return StartCoroutine("LoadScene");
		PopulateData();

		CreateGenerator();

		terrainState.Initialize();
		if (loadingState.PreloadChunks || loadingState.MeasurePerformance)
		{
			yield return StartCoroutine("PreloadChunks");
		}
		else
		{
			yield return StartCoroutine("LoadSurround");
		}

		GameTransition();
	}

	private class DisabledMeasure : IMeasure
	{
		IDisposable IMeasure.Measure(string category) => null;

		private class Watch : IDisposable
		{
			private readonly string category;
			private readonly Stopwatch watch;

			public Watch(string category)
			{
				this.category = category;
				watch = Stopwatch.StartNew();
			}

			void IDisposable.Dispose() => Debug.Log($"{category}: {watch.Elapsed}");
		}
	}

	private class PerformanceMeasure : IMeasure
	{
		private readonly IPerformance performance;

		public PerformanceMeasure(IPerformance performance)
		{
			this.performance = performance;
		}

		IDisposable IMeasure.Measure(string category) => new Watch(this, category);

		private class Watch : IDisposable
		{
			private readonly string categroy;
			private readonly PerformanceMeasure measure;
			private readonly Stopwatch watch;

			public Watch(PerformanceMeasure measure, string categroy)
			{
				this.measure = measure;
				this.categroy = categroy;
				watch = Stopwatch.StartNew();
			}

			void IDisposable.Dispose()
			{
				measure.performance.Log(categroy, watch.Elapsed);
			}
		}
	}
}
