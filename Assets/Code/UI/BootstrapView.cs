using UnityEngine;
using UnityEngine.UI;

public class BootstrapView : View
{
	[SerializeField]
	private View nextView;

	private void OnEnable()
	{
		ViewStack.GetComponent<CanvasScaler>().uiScaleMode = CanvasScaler.ScaleMode.ConstantPhysicalSize;
		ViewStack.Replace(nextView);
	}
}
