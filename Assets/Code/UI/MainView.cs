using System;
using System.Runtime.InteropServices;
using TerrainGenerator;
using TerrainGenerator.Data.Assets;
using UnityEngine;
using UnityEngine.UI;

public class MainView : View
{
	public BiomeSetAsset biomeSet;
	public TileProvider noiseGenerator;
	public TileProvider polygonalGenerator;
	[SerializeField]
	private View loadingView = null;
	[SerializeField]
	private Toggle performanceMonitoring = null;
	[SerializeField]
	private Toggle preloadChunks = null;
	[SerializeField]
	private InputField seedInput = null;
	[SerializeField]
	private SizeFactors sizeFactors;
	[SerializeField]
	private Sizes sizes;
	[SerializeField]
	private Slider viewDistance = null;
	[SerializeField]
	private Dropdown worldGenerator = null;
	[SerializeField]
	private Dropdown worldSize = null;

	public void StartGame()
	{
		var state = LoadingState.Instance;
		state.BiomeSet = biomeSet;
		state.SelectedSize = ConvertSize();
		state.SelectedSizeFactor = ConvertSizeFactor();
		state.SelectedSizeName = ((SizeNames)worldSize.value).ToString();
		state.MeasurePerformance = performanceMonitoring.isOn;
		state.PreloadChunks = preloadChunks.isOn;

		switch (worldGenerator.value)
		{
			case 0:
				state.SelectedTileProvider = noiseGenerator;
				break;

			case 1:
				state.SelectedTileProvider = polygonalGenerator;
				break;

			default:
				return;
		}

		ViewStack.Replace(loadingView);
	}

	private Vector2Int ConvertSize()
	{
		switch (worldSize.value)
		{
			case 0:
				return sizes.Mini;

			case 1:
				return sizes.Small;

			case 2:
				return sizes.Medium;

			case 3:
				return sizes.Large;

			case 4:
				return sizes.Huge;

			default:
				throw new Exception();
		}
	}

	private float ConvertSizeFactor()
	{
		switch (worldSize.value)
		{
			case 0:
				return sizeFactors.Mini;

			case 1:
				return sizeFactors.Small;

			case 2:
				return sizeFactors.Medium;

			case 3:
				return sizeFactors.Large;

			case 4:
				return sizeFactors.Huge;

			default:
				throw new Exception();
		}
	}

	[Serializable]
	[StructLayout(LayoutKind.Auto)]
	private struct SizeFactors
	{
		public float Huge;
		public float Large;
		public float Medium;
		public float Small;
		public float Mini;
	}

	private enum SizeNames
	{
		Mini,
		Small,
		Medium,
		Large,
		Huge
	}

	[Serializable]
	[StructLayout(LayoutKind.Auto)]
	private struct Sizes
	{
		public Vector2Int Huge;
		public Vector2Int Large;
		public Vector2Int Medium;
		public Vector2Int Small;
		public Vector2Int Mini;
	}
}
