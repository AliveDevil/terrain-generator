using System;
using TerrainGenerator;
using TerrainGenerator.Data.Assets;
using UnityEngine;

public class LoadingState : IDisposable
{
	public BiomeSetAsset BiomeSet;
	public bool MeasurePerformance;
	public bool PreloadChunks;
	public string Seed;
	public Vector2Int SelectedSize;
	public float SelectedSizeFactor;
	public string SelectedSizeName;
	public TileProvider SelectedTileProvider;
	private static LoadingState instance;

	public static LoadingState Instance => instance ?? (instance = new LoadingState());

	public void Dispose()
	{
		instance = null;

		BiomeSet = null;
		SelectedTileProvider = null;
	}
}
