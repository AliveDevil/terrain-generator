using TerrainGenerator;
using TerrainGenerator.Core;
using UnityEngine;

public class GameManager : MonoBehaviour
{
	private GameState gameState;
	private InputState inputState;
	private TerrainState terrainState;

	public GameState GameState => gameState;

	public InputState InputState => inputState;

	public TerrainState TerrainState => terrainState;

	public void Run()
	{
		InputState.InputManager.Begin();

		terrainState.TerrainController.Run();
	}

	private void OnEnable()
	{
		gameState = GameState.PerScene(gameObject);
		terrainState = TerrainState.PerScene(gameObject);
		inputState = InputState.PerScene(gameObject);

		gameState.TerrainState = terrainState;
		gameState.InputState = inputState;

		gameState.GameManager = this;
	}
}
