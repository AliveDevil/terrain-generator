using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks.Dataflow;
using UnityEngine;

namespace TerrainGenerator
{
	public sealed class AppState
	{
		public static AppState State { get; private set; }

		public IPerformance GetPerformance(string generator, string size)
		{
			var fileName = $"Performance-{generator}-{size}-{SystemInfo.deviceUniqueIdentifier}-{DateTime.Now:yyyy-mm-dd-hh-mm-ss}.csv";
			var file = new FileInfo(Path.Combine(Application.persistentDataPath, fileName));
			if (!file.Directory.Exists)
			{
				file.Directory.Create();
			}
			var run = new PerformanceRun(file);
			run.Log("Id;Record;Time");
			return run;
		}

		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
		private static void Setup()
		{
			State = new AppState();
		}

		private class PerformanceRun : IPerformance
		{
			private ActionBlock<string> actions;
			private BufferBlock<string> buffer;
			private FileStream fileStream;
			private int logEntry = 0;
			private StreamWriter writer;

			public PerformanceRun(FileInfo file)
			{
				fileStream = file.Open(FileMode.Create, FileAccess.Write, FileShare.Read);
				writer = new StreamWriter(fileStream) { AutoFlush = true };
				buffer = new BufferBlock<string>(new DataflowBlockOptions() { EnsureOrdered = true });
				actions = new ActionBlock<string>(s => writer.WriteLineAsync(s), new ExecutionDataflowBlockOptions() { EnsureOrdered = true });
				buffer.LinkTo(actions);
			}

			public void Dispose()
			{
				buffer.Complete();
				buffer.Completion.ContinueWith(t => actions.Complete());
				actions.Completion.Wait();
				writer.Flush();
				writer.Dispose();
			}

			public void Log(string message)
			{
				buffer.SendAsync($"{message}");
			}

			public void Log(string message, TimeSpan time)
			{
				var id = Interlocked.Increment(ref logEntry);
				buffer.SendAsync($"{id};{message};{time.TotalMilliseconds}");
			}
		}
	}

	public interface IPerformance : IDisposable
	{
		void Log(string message);

		void Log(string message, TimeSpan time);
	}
}
