using System;
using System.Collections;
using System.Collections.Generic;

namespace TerrainGenerator.Core
{
	public class Deque<T> : IEnumerable<T>
	{
		private const int DefaultCapacity = 8;
		private T[] buffer;
		private int offset;

		public int Capacity
		{
			get
			{
				return buffer.Length;
			}
			set
			{
				if (value < Count)
				{
					return;
				}

				if (value == buffer.Length)
				{
					return;
				}

				T[] newBuffer = new T[value];
				CopyToArray(newBuffer);

				buffer = newBuffer;
				offset = 0;
			}
		}

		public int Count { get; private set; }

		private bool IsEmpty
		{
			get { return Count == 0; }
		}

		private bool IsFull
		{
			get { return Count == Capacity; }
		}

		private bool IsSplit
		{
			get
			{
				// Overflow-safe version of "(offset + Count) > Capacity"
				return offset > (Capacity - Count);
			}
		}

		public Deque() : this(DefaultCapacity)
		{
		}

		public Deque(int capacity)
		{
		}

		public Deque(IEnumerable<T> collection)
		{
		}

		public void AddToBack(T value)
		{
			EnsureCapacityForOneElement();
			DoAddToBack(value);
		}

		public void AddToFront(T value)
		{
			EnsureCapacityForOneElement();
			DoAddToFront(value);
		}

		public void Clear()
		{
			offset = 0;
			Count = 0;
		}

		public T Dequeue()
		{
			return RemoveFromFront();
		}

		public void Enqueue(T value)
		{
			AddToBack(value);
		}

		public IEnumerator<T> GetEnumerator()
		{
			return new DequeEnumerator(this);
		}

		public T Pop()
		{
			return RemoveFromBack();
		}

		public void Push(T value)
		{
			AddToBack(value);
		}

		public T RemoveFromBack()
		{
			if (IsEmpty)
				return default(T);

			return DoRemoveFromBack();
		}

		public T RemoveFromFront()
		{
			if (IsEmpty)
				return default(T);

			return DoRemoveFromFront();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		private void CopyToArray(Array array, int arrayIndex = 0)
		{
			if (array == null)
				throw new ArgumentNullException("array");

			if (IsSplit)
			{
				// The existing buffer is split, so we have to copy it in parts
				int length = Capacity - offset;
				Array.Copy(buffer, offset, array, arrayIndex, length);
				Array.Copy(buffer, 0, array, arrayIndex + length, Count - length);
			}
			else
			{
				// The existing buffer is whole
				Array.Copy(buffer, offset, array, arrayIndex, Count);
			}
		}

		private int DequeIndexToBufferIndex(int index)
		{
			return (index + offset) % Capacity;
		}

		private void DoAddToBack(T value)
		{
			buffer[DequeIndexToBufferIndex(Count)] = value;
			++Count;
		}

		private void DoAddToFront(T value)
		{
			buffer[PreDecrement(1)] = value;
			++Count;
		}

		private T DoGetItem(int index)
		{
			return buffer[DequeIndexToBufferIndex(index)];
		}

		private T DoRemoveFromBack()
		{
			T ret = buffer[DequeIndexToBufferIndex(Count - 1)];
			--Count;
			return ret;
		}

		private T DoRemoveFromFront()
		{
			--Count;
			return buffer[PostIncrement(1)];
		}

		private void DoSetItem(int index, T item)
		{
			buffer[DequeIndexToBufferIndex(index)] = item;
		}

		private void EnsureCapacityForOneElement()
		{
			if (IsFull)
			{
				Capacity = (Capacity == 0) ? 1 : Capacity * 2;
			}
		}

		private int PostIncrement(int value)
		{
			int ret = offset;
			offset += value;
			offset %= Capacity;
			return ret;
		}

		private int PreDecrement(int value)
		{
			offset -= value;
			if (offset < 0)
				offset += Capacity;
			return offset;
		}

		private struct DequeEnumerator : IEnumerator<T>
		{
			private int count;
			private Deque<T> deque;
			private int index;
			private int nextIndex;
			private int offset;

			public T Current
			{
				get
				{
					return deque.DoGetItem(index);
				}
			}

			object IEnumerator.Current { get { return Current; } }

			public DequeEnumerator(Deque<T> deque)
			{
				offset = deque.offset;
				count = deque.Count;
				index = 0;
				nextIndex = 0;
				this.deque = deque;
			}

			public void Dispose()
			{
				deque = null;
			}

			public bool MoveNext()
			{
				if (deque.Count != count)
				{
					return false;
				}
				if (deque.offset != offset)
				{
					return false;
				}
				if (nextIndex >= count)
				{
					return false;
				}
				index = nextIndex;
				nextIndex += 1;
				return true;
			}

			public void Reset()
			{
				index = 0;
				nextIndex = 0;
			}
		}
	}
}
