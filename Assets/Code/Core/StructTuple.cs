namespace TerrainGenerator.Core
{
	public static class StructTuple
	{
		public static StructTuple<T1> Create<T1>(T1 item1)
		{
			return new StructTuple<T1>(item1);
		}

		public static StructTuple<T1, T2> Create<T1, T2>(T1 item1, T2 item2)
		{
			return new StructTuple<T1, T2>(item1, item2);
		}

		public static StructTuple<T1, T2, T3> Create<T1, T2, T3>(T1 item1, T2 item2, T3 item3)
		{
			return new StructTuple<T1, T2, T3>(item1, item2, item3);
		}

		public static StructTuple<T1, T2, T3, T4> Create<T1, T2, T3, T4>(T1 item1, T2 item2, T3 item3, T4 item4)
		{
			return new StructTuple<T1, T2, T3, T4>(item1, item2, item3, item4);
		}
	}

	public struct StructTuple<T1>
	{
		public T1 Item1;

		public StructTuple(T1 item1)
		{
			Item1 = item1;
		}
	}

	public struct StructTuple<T1, T2>
	{
		public T1 Item1;
		public T2 Item2;

		public StructTuple(T1 item1, T2 item2)
		{
			Item1 = item1;
			Item2 = item2;
		}
	}

	public struct StructTuple<T1, T2, T3>
	{
		public T1 Item1;
		public T2 Item2;
		public T3 Item3;

		public StructTuple(T1 item1, T2 item2, T3 item3)
		{
			Item1 = item1;
			Item2 = item2;
			Item3 = item3;
		}
	}

	public struct StructTuple<T1, T2, T3, T4>
	{
		public T1 Item1;
		public T2 Item2;
		public T3 Item3;
		public T4 Item4;

		public StructTuple(T1 item1, T2 item2, T3 item3, T4 item4)
		{
			Item1 = item1;
			Item2 = item2;
			Item3 = item3;
			Item4 = item4;
		}
	}
}
