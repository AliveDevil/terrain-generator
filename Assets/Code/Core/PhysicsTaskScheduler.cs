using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace TerrainGenerator.Core
{
	public class PhysicsTaskScheduler : TaskScheduler
	{
		private readonly LinkedList<Task> queue = new LinkedList<Task>();
		private readonly SynchronizationContext unitySynchronizationContext;

		public PhysicsTaskScheduler(SynchronizationContext context)
		{
			unitySynchronizationContext = context;
		}

		public void ExecutePendingTasks()
		{
			while (true)
			{
				Task task;
				lock (queue)
				{
					if (queue.Count == 0)
					{
						break;
					}

					task = queue.First.Value;
					queue.RemoveFirst();
				}

				if (task != null)
				{
					var result = TryExecuteTask(task);
					if (result == false)
					{
						throw new InvalidOperationException();
					}
					break;
				}
			}
		}

		protected override IEnumerable<Task> GetScheduledTasks()
		{
			lock (queue)
			{
				return queue.ToArray();
			}
		}

		protected override void QueueTask(Task task)
		{
			lock (queue)
			{
				queue.AddLast(task);
			}
		}

		protected override bool TryExecuteTaskInline(Task task, bool taskWasPreviouslyQueued)
		{
			if (SynchronizationContext.Current != unitySynchronizationContext)
			{
				return false;
			}

			if (taskWasPreviouslyQueued)
			{
				lock (queue)
				{
					queue.Remove(task);
				}
			}

			return TryExecuteTask(task);
		}
	}
}
