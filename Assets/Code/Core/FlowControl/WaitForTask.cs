using System.Threading.Tasks;
using UnityEngine;

namespace TerrainGenerator.Core.FlowControl
{
	public class WaitForTask : CustomYieldInstruction
	{
		private readonly Task task;

		public override bool keepWaiting
		{
			get
			{
				if (task.IsFaulted)
				{
					throw task.Exception;
				}
				return !task.IsCompleted;
			}
		}

		public WaitForTask(Task task)
		{
			this.task = task;
		}
	}
}
