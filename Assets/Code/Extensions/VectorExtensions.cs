using UnityEngine;

namespace TerrainGenerator.Extensions
{
	public static class VectorExtensions
	{
		public static Vector3 ToV3(this Vector2 v)
		{
			return new Vector3(v.x, 0, v.y);
		}

		public static Vector3 ToV3(this Vector2 v, float h)
		{
			return new Vector3(v.x, h, v.y);
		}
	}
}
