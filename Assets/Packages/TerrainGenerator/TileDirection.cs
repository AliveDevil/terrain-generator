using System;

namespace TerrainGenerator
{
	[Flags]
	public enum TileDirection
	{
		North = 1, South = -1,
		East = 2, West = -2,
	}
}
