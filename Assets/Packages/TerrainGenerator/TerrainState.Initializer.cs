using System;
using System.Collections.Generic;
using TerrainGenerator.Contracts;

namespace TerrainGenerator
{
	public partial class TerrainState
	{
		private Dictionary<IState, HashSet<Type>> depends = new Dictionary<IState, HashSet<Type>>();
		private List<IState> states = new List<IState>();

		public void Depends(IState state, Type type)
		{
			depends[state].Add(type);
		}

		public void Initialize()
		{
			SortStates();
			for (int i = 0, n = states.Count; i < n; i++)
			{
				states[i].Initialize();
				states[i] = null;
			}
			states.Clear();
		}

		public void Register(IState state)
		{
			states.Add(state);
			if (!depends.ContainsKey(state))
			{
				depends[state] = new HashSet<Type>();
			}
		}

		private void SortStates()
		{
			states.Sort((left, right) =>
			{
				HashSet<Type> leftTypes;
				if (depends.TryGetValue(left, out leftTypes))
				{
					foreach (var item in leftTypes)
					{
						if (item.IsInstanceOfType(right))
						{
							return -1;
						}
					}
				}
				HashSet<Type> rightTypes;
				if (depends.TryGetValue(right, out rightTypes))
				{
					foreach (var item in rightTypes)
					{
						if (item.IsInstanceOfType(left))
						{
							return 1;
						}
					}
				}
				return 0;
			});
			foreach (var item in depends.Keys)
			{
				depends[item].Clear();
			}
			depends.Clear();
		}
	}
}
