using FieldTreeStructure.Spatial;
using TerrainGenerator.Data.Assets;
using UnityEngine;

namespace TerrainGenerator.Data
{
	public interface IStructureBuilder
	{
		Structure Build();

		IStructureBuilder WithAsset(StructureAsset asset);

		IStructureBuilder WithInstance(GameObject instance);

		IStructureBuilder WithPosition(int x, int y);

		IStructureBuilder WithPosition(Vector2Int position);

		IStructureBuilder WithSize(int width, int height);

		IStructureBuilder WithSize(Vector2Int size);
	}

	public class Structure : ISpatial
	{
		private StructureAsset asset;
		private GameObject instance;
		private Vector2Int position;
		private Vector2Int size;

		public StructureAsset Asset => asset;

		public int CenterX => position.x;

		public int CenterY => position.y;

		public int Height => size.y;

		public GameObject Instance => instance;

		public int Width => size.x;

		public static IStructureBuilder Build() => new StructureBuilder();

		public bool Equals(ISpatial other)
		{
			return false;
		}

		private class StructureBuilder : IStructureBuilder
		{
			private StructureAsset asset;
			private GameObject instance;
			private Vector2Int position;
			private Vector2Int size;

			Structure IStructureBuilder.Build() => new Structure()
			{
				asset = asset,
				instance = instance,
				position = position,
				size = size
			};

			IStructureBuilder IStructureBuilder.WithAsset(StructureAsset asset)
			{
				this.asset = asset;
				return this;
			}

			IStructureBuilder IStructureBuilder.WithInstance(GameObject instance)
			{
				this.instance = instance;
				return this;
			}

			IStructureBuilder IStructureBuilder.WithPosition(int x, int y)
			{
				position = new Vector2Int(x, y);
				return this;
			}

			IStructureBuilder IStructureBuilder.WithPosition(Vector2Int position)
			{
				this.position = position;
				return this;
			}

			IStructureBuilder IStructureBuilder.WithSize(int width, int height)
			{
				size = new Vector2Int(width, height);
				return this;
			}

			IStructureBuilder IStructureBuilder.WithSize(Vector2Int size)
			{
				this.size = size;
				return this;
			}
		}
	}
}
