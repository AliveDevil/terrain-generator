using UnityEngine;

namespace TerrainGenerator.Data.Assets
{
	public class BiomeAsset : ScriptableObject
	{
		[SerializeField]
		private Color color = Color.gray;
		[SerializeField]
		private float meanRainfall = 0;
		//[SerializeField]
		//private float meanHumidity = 0;
		[SerializeField]
		private float meanTemperature = 0;
		[SerializeField]
		private BiomeStructureDefinition[] structureDefinitions;
		[SerializeField]
		private int terrainAmplitude = 15;
		//[SerializeField]
		//private float maxHumidity = 0;
		//[SerializeField]
		//private float maxRainfall = 0;
		//[SerializeField]
		//private float maxTemperature = 0;
		//[SerializeField]
		//private float minHumidity = 0;
		//[SerializeField]
		//private float minRainfall = 0;
		//[SerializeField]
		//private float minTemperature = 0;

		public Color Color { get { return color; } }

		public float MeanRainfall { get { return meanRainfall; } }

		public float MeanTemperature { get { return meanTemperature; } }

		public BiomeStructureDefinition[] StructureDefinitions { get { return structureDefinitions; } }

		public int TerrainAmplitude { get { return terrainAmplitude; } }
	}
}
