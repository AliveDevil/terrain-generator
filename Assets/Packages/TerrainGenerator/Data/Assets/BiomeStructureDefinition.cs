using System;
using TerrainGenerator.Contracts;
using UnityEngine;

namespace TerrainGenerator.Data.Assets
{
	[Serializable]
	public struct BiomeStructureDefinition
	{
		[Range(0, 1)]
		public float chance;
		public int densityRadius;
		public StructureAsset structure;

		public void Place(IStructurePlacement placement)
		{
			placement.WithBiomeStructureDefinition(ref this);

			structure.Place(placement);
		}
	}
}
