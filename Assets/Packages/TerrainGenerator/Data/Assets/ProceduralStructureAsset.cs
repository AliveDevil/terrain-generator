using TerrainGenerator.Contracts;
using UnityEngine;

namespace TerrainGenerator.Data.Assets
{
	public class ProceduralStructureAsset : StructureAsset
	{
		[SerializeField]
		private SimpleStructureAsset simpleStructure = null;

		protected override bool OnCanSpawn(ITile tile, Vector2Int position, IStructureGeneratorContext structureGenerator) => false;

		protected override void OnPlace(IStructurePlacement placement)
		{
		}

		protected override void OnPostProcess()
		{
		}

		protected override void OnSpawn(IStructureSpawn spawn)
		{
			simpleStructure.Spawn(spawn);
		}
	}
}
