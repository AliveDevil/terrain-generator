using TerrainGenerator.Contracts;
using UnityEngine;

namespace TerrainGenerator.Data.Assets
{
	public abstract class StructureAsset : ScriptableObject
	{
		public bool CanSpawn(ITile tile, Vector2Int position, IStructureGeneratorContext structureGenerator) => OnCanSpawn(tile, position, structureGenerator);

		public void Place(IStructurePlacement placement) => OnPlace(placement);

		public void PostProcess() => OnPostProcess();

		public void Spawn(IStructureSpawn spawn) => OnSpawn(spawn);

		protected abstract bool OnCanSpawn(ITile tile, Vector2Int position, IStructureGeneratorContext structureGenerator);

		protected abstract void OnPlace(IStructurePlacement placement);

		protected abstract void OnPostProcess();

		protected abstract void OnSpawn(IStructureSpawn spawn);
	}
}
