using UnityEngine;

namespace TerrainGenerator.Data.Assets
{
	public class BiomeSetAsset : ScriptableObject
	{
		public BiomeAsset[] Biomes;
		public BiomeAsset Ocean;
	}
}
