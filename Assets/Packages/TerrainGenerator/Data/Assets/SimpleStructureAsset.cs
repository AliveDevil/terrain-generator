using TerrainGenerator.Contracts;
using UnityEngine;

namespace TerrainGenerator.Data.Assets
{
	public class SimpleStructureAsset : StructureAsset
	{
		[SerializeField]
		private GameObject prefab = null;
		[SerializeField]
		private Vector2Int size;

		protected override bool OnCanSpawn(ITile tile, Vector2Int position, IStructureGeneratorContext structureGenerator) => true;

		protected override void OnPlace(IStructurePlacement placement)
		{
			placement.WithSize(size);
		}

		protected override void OnPostProcess()
		{
		}

		protected override void OnSpawn(IStructureSpawn spawn)
		{
			spawn.WithSize(size);
			spawn.WithPrefab(prefab);
			spawn.WithStructure(this);
			spawn.WithRotation(0);
		}
	}
}
