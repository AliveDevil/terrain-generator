using System.Threading;
using System.Threading.Tasks;
using TerrainGenerator.Contracts;
using UnityEngine;

namespace TerrainGenerator
{
	public partial class TerrainState : MonoBehaviour
	{
		public IBiomeGraph BiomeGraph;
		public TaskFactory GeneratorBackgroundTaskFactory;
		public TaskScheduler GeneratorBackgroundTaskScheduler;
		public TaskFactory MainThreadTaskFactory;
		public TaskScheduler MainThreadTaskScheduler;
		public IMeasure Measure;
		public IMeshGenerator MeshGenerator;
		public IMeshPool MeshPool;
		public TaskFactory PhysicsTaskFactory;
		public TaskScheduler PhysicsTaskScheduler;
		public int Seed;
		public RectInt Size;
		public float SizeFactor;
		public IStructureGenerator StructureGenerator;
		public TaskFactory TaskFactory;
		public ITerrainController TerrainController;
		public ITerrainGenerator TerrainGenerator;
		public GameObject TerrainObject;
		public Transform TerrainTransform;
		public ITileProvider TileProvider;
		private CancellationTokenSource cancelTokenSource = new CancellationTokenSource();

		public CancellationToken CancellationToken => cancelTokenSource.Token;

		public string SeedString => Seed.ToString("X8");

		public void Halt()
		{
			cancelTokenSource.Cancel();
			cancelTokenSource.Dispose();
		}
	}
}
