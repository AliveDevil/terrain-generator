using TerrainGenerator.Data.Assets;
using UnityEngine;

namespace TerrainGenerator
{
	public class TerrainSample
	{
		public BiomeAsset Biome { get; set; }

		public int Elevation { get; set; }

		public Vector2Int Position { get; }

		public float Rainfall { get; set; }

		public bool Surface { get; set; }

		public float Temperature { get; set; }

		public TerrainSample(Vector2Int position)
		{
			Position = position;
		}
	}
}
