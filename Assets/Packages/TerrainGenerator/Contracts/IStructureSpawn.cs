using TerrainGenerator.Data.Assets;
using UnityEngine;

namespace TerrainGenerator.Contracts
{
	public interface IStructureSpawn
	{
		void Spawn();

		IStructureSpawn WithPosition(Vector2Int position);

		IStructureSpawn WithPrefab(GameObject prefab);

		IStructureSpawn WithRotation(float rotation);

		IStructureSpawn WithSize(Vector2Int size);

		IStructureSpawn WithStructure(StructureAsset asset);
	}
}
