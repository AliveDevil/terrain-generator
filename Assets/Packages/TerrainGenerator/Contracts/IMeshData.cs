using UnityEngine;

namespace TerrainGenerator.Contracts
{
	public interface IMeshData
	{
		void AddQuad(bool collision, bool sync);

		void AddQuadColor(Color color);

		void AddVertex(Vector3 vertex, bool collision, bool sync);

		void Begin();

		void Fill(Mesh mesh, Mesh collisionMesh);
	}

	public interface IMeshDataContainer
	{
		IMeshData Fluid { get; }

		IMeshData Solid { get; }

		void Begin();

		void Fill(Mesh solidMesh, Mesh fluidMesh, Mesh solidCollisionMesh, Mesh fluidCollisionMesh);
	}
}
