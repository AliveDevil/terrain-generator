using TerrainGenerator.Data.Assets;

namespace TerrainGenerator.Contracts
{
	public interface ITile
	{
		BiomeAsset Biome { get; set; }
	}
}
