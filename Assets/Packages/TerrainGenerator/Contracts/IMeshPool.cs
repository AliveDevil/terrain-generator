using UnityEngine;

namespace TerrainGenerator.Contracts
{
	public interface IMeshPool
	{
		Mesh Pop();

		void Push(Mesh mesh);
	}
}
