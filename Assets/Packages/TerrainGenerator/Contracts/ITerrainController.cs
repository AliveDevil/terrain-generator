using System.Threading.Tasks;
using UnityEngine;

namespace TerrainGenerator.Contracts
{
	public interface ITerrainController : IState
	{
		int ChunkSize { get; }

		RectInt Size { get; }

		Vector2Int GetChunkIndex(Vector2 v);

		Vector2Int GetChunkIndex(Vector3 v);

		Vector2Int GetChunkIndex(Vector2Int v);

		Vector2Int GetTileIndex(Vector2 v);

		Vector2Int GetTileIndex(Vector3 v);

		Vector2Int GetTileIndex(Vector2Int v);

		Task LoadChunk(Vector2Int chunkIndex);

		void Run();

		ITile Sample(Vector2Int position);
	}
}
