using TerrainGenerator.Data.Assets;
using UnityEngine;

namespace TerrainGenerator.Contracts
{
	public interface IStructurePlacement
	{
		IStructurePlacement WithBiomeStructureDefinition(ref BiomeStructureDefinition structureDefinition);

		IStructurePlacement WithDensity(int density);

		IStructurePlacement WithPosition(Vector2Int position);

		IStructurePlacement WithSize(Vector2Int size);

		IStructurePlacement WithStructure(StructureAsset structure);
	}
}
