using System;

namespace TerrainGenerator.Contracts
{
	public interface IMeasure
	{
		IDisposable Measure(string category);
	}
}
