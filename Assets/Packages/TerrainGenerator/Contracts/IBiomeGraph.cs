using System;
using TerrainGenerator.Data.Assets;

namespace TerrainGenerator.Contracts
{
	public interface IBiomeGraph : IState
	{
		BiomeSetAsset BiomeSet { get; set; }

		BiomeAsset Ocean { get; }

		[Obsolete]
		void Build();

		BiomeAsset Find(float temperature, float rainfall);
	}
}
