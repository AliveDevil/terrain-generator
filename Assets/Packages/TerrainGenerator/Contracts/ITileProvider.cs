namespace TerrainGenerator.Contracts
{
	public interface ITileProvider : IState
	{
		void Elevate(TerrainSample sample);

		void Sample(TerrainSample sample);
	}
}
