using TerrainGenerator.Data.Assets;
using UnityEngine;

namespace TerrainGenerator.Contracts
{
	public interface IStructureContainer
	{
		IStructureSpawn Spawn();

		void Spawn(Vector2Int position, Vector2Int size, float rotation, StructureAsset structureAsset, GameObject prefab);
	}
}
