using System.Collections.Generic;
using System.Threading.Tasks;
using TerrainGenerator.Core;

namespace TerrainGenerator.Contracts
{
	public interface IMeshGenerator
	{
		Task GenerateMeshAsync(IMeshDataContainer meshData, IEnumerator<TileItem> enumerable);
	}
}
