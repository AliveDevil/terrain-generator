using System;
using System.Threading.Tasks;
using UnityEngine;

namespace TerrainGenerator.Contracts
{
	public interface ITerrainGenerator : IState
	{
		void Generate(Vector2Int chunkIndex, Action<Vector2Int, ITile> callback);

		Task GenerateAsync(Vector2Int chunkIndex, Action<Vector2Int, ITile> callback);
	}
}
