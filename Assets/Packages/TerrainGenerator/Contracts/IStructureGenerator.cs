using System;
using UnityEngine;

namespace TerrainGenerator.Contracts
{
	public interface IStructureGenerator : IState
	{
		void Generate(IStructureContainer structureContainer, Vector2Int chunkPosition, int chunkSize, Func<Vector2Int, ITile> tileAccessor, int initializer);
	}

	public interface IStructureGeneratorContext
	{
	}
}
