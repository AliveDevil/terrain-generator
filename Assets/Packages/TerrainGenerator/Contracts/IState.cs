namespace TerrainGenerator.Contracts
{
	public interface IState
	{
		void Initialize();
	}
}
