using TerrainGenerator.Contracts;
using UnityEngine;

namespace TerrainGenerator.Core
{
	public abstract class TileItem
	{
		public Vector2Int Location { get; }

		public ITile Tile { get; }

		public TileItem(ITile tile, Vector2Int location)
		{
			Tile = tile;
			Location = location;
		}

		public abstract ITile GetTile(TileDirection direction);
	}
}
