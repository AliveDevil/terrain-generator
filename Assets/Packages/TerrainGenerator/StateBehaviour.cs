using TerrainGenerator.Contracts;
using UnityEngine;

namespace TerrainGenerator
{
	public abstract class StateBehaviour : MonoBehaviour, IState
	{
		private TerrainState terrainState;

		protected TerrainState TerrainState => terrainState ?? (terrainState = gameObject.GetTerrainState());

		public void Initialize()
		{
			OnInitialize();
		}

		protected void Depends<T>() where T : IState
		{
			TerrainState.Depends(this, typeof(T));
		}

		protected void OnDisable()
		{
			OnTearDown();
		}

		protected void OnEnable()
		{
			OnSetup();
			TerrainState.Register(this);
			OnRegister();
		}

		protected abstract void OnInitialize();

		protected abstract void OnRegister();

		protected abstract void OnSetup();

		protected abstract void OnTearDown();
	}
}
