using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace TerrainGenerator
{
	public static class TerrainStateExtensions
	{
		public static TerrainState GetTerrainState(this GameObject gameObject) => TerrainState.PerScene(gameObject);
	}

	public partial class TerrainState
	{
		private static Dictionary<int, TerrainState> perSceneIndex;

		public static TerrainState Active => GetOrCreateTerrainState(SceneManager.GetActiveScene());

		public static TerrainState PerScene(GameObject gameObject) => GetOrCreateTerrainState(gameObject.scene);

		private static void Application_quitting()
		{
			Application.quitting -= Application_quitting;
			SceneManager.sceneUnloaded -= SceneManager_sceneUnloaded;

			foreach (var item in perSceneIndex)
			{
				item.Value.Halt();
				Destroy(item.Value);
			}
			perSceneIndex.Clear();
		}

		private static TerrainState GetOrCreateTerrainState(Scene scene)
		{
			TerrainState terrainState;
			var sceneIndex = scene.GetHashCode();
			if (!perSceneIndex.TryGetValue(sceneIndex, out terrainState))
			{
				var gameObject = new GameObject();
				SceneManager.MoveGameObjectToScene(gameObject, scene);
				terrainState = gameObject.AddComponent<TerrainState>();
				perSceneIndex[sceneIndex] = terrainState;
			}
			return terrainState;
		}

		private static void SceneManager_sceneUnloaded(Scene scene)
		{
			TerrainState state;
			if (perSceneIndex.TryGetValue(scene.GetHashCode(), out state))
			{
				state.Halt();
				Destroy(state.gameObject);
			}
			perSceneIndex.Remove(scene.GetHashCode());
		}

		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
		private static void Setup()
		{
			perSceneIndex = new Dictionary<int, TerrainState>();
			Application.quitting += Application_quitting;
			SceneManager.sceneUnloaded += SceneManager_sceneUnloaded;
		}
	}
}
