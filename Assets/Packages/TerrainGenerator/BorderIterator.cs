using System.Collections.Generic;
using UnityEngine;

namespace TerrainGenerator
{
	public static class BorderIterator
	{
		public static IEnumerable<Vector2Int> Border(Vector2Int center, int radius)
		{
			yield return center;
			for (int d = 1; d < (radius + 1); d++)
			{
				yield return new Vector2Int(center.x, center.y + d);
				yield return new Vector2Int(center.x + d, center.y);
				yield return new Vector2Int(center.x - d, center.y);
				yield return new Vector2Int(center.x, center.y - d);

				for (int p = 1; p < d + 1; p++)
				{
					yield return new Vector2Int(center.x + p, center.y + d);
					yield return new Vector2Int(center.x - p, center.y + d);
					yield return new Vector2Int(center.x + p, center.y - d);
					yield return new Vector2Int(center.x - p, center.y - d);
					yield return new Vector2Int(center.x + d, center.y + p);
					yield return new Vector2Int(center.x + d, center.y - p);
					yield return new Vector2Int(center.x - d, center.y + p);
					yield return new Vector2Int(center.x - d, center.y - p);
				}
			}
		}
	}
}
