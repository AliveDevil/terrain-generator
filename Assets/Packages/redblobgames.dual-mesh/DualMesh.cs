using d3.delaunay;
using System;
using System.Linq;
using UnityEngine;

namespace redblobgames
{
	public static class DualMeshExtensions
	{
		public static DualMesh DualMesh(this Delaunay delaunay)
		{
			return new DualMesh(delaunay);
		}
	}

	public partial class DualMesh
	{
		private Delaunay delaunay;
		private int[] halfedges;
		private int numRegions;
		private int numSides;
		private int numSolidRegions;
		private int numSolidSides;
		private int numSolidTriangles;
		private int numTriangles;
		private int[] r_any_s;
		private Vector2[] r_vertex;
		private Vector2[] t_vertex;
		private int[] triangles;

		public DualMesh(Delaunay delaunay)
		{
			this.delaunay = delaunay;

			var points = delaunay.Points;
			var halfedges = delaunay.Halfedges;
			var triangles = delaunay.Triangles;

			numSolidSides = triangles.Length;
			var ghost_r = points.Length;

			var numUnpairedSides = 0;
			var firstUnpairedEdge = -1;
			var r_unpaired_s = new int[0];

			for (int s = 0; s < numSolidSides; s++)
			{
				if (halfedges[s] == -1)
				{
					numUnpairedSides++;
					Set(ref r_unpaired_s, triangles[s], s);
					firstUnpairedEdge = s;
				}
			}

			var s_newstart_r = new int[numSolidSides + 3 * numUnpairedSides];
			Array.Copy(triangles, s_newstart_r, triangles.Length);
			var s_newopposite_s = new int[numSolidSides + 3 * numUnpairedSides];
			Array.Copy(halfedges, s_newopposite_s, halfedges.Length);

			for (int i = 0, s = firstUnpairedEdge;
				i < numUnpairedSides;
				i++, s = r_unpaired_s[s_newstart_r[s_next_s(s)]])
			{
				var ghost_s = numSolidSides + 3 * i;
				s_newopposite_s[s] = ghost_s;
				s_newopposite_s[ghost_s] = s;
				s_newstart_r[ghost_s] = s_newstart_r[s_next_s(s)];

				s_newstart_r[ghost_s + 1] = s_newstart_r[s];
				s_newstart_r[ghost_s + 2] = ghost_r;
				var k = numSolidSides + (3 * i + 4) % (3 * numUnpairedSides);
				s_newopposite_s[ghost_s + 2] = k;
				s_newopposite_s[k] = ghost_s + 2;
			}

			this.r_vertex = points;
			this.triangles = triangles = s_newstart_r;
			this.halfedges = halfedges = s_newopposite_s;

			numSides = triangles.Length;
			numRegions = points.Length;
			numSolidRegions = numRegions;
			numTriangles = numSides / 3;
			numSolidTriangles = numSolidSides / 3;

			r_any_s = Enumerable.Repeat(-1, numRegions).ToArray();
			for (int s = 0; s < triangles.Length; s++)
			{
				if (!r_ghost(triangles[s]) && r_any_s[triangles[s]] == -1)
				{
					r_any_s[triangles[s]] = s;
				}
			}

			t_vertex = new Vector2[numTriangles];
			for (int s = 0; s < triangles.Length; s += 3)
			{
				if (s_ghost(s))
				{
					var a = r_vertex[triangles[s]];
					var b = r_vertex[triangles[s + 1]];

					var dx = b.x - a.x;
					var dy = b.y - a.y;
					t_vertex[s / 3] = new Vector2(a.x + 0.5f * (dx + dy), a.y + 0.5f * (dy - dx));
				}
				else
				{
					var a = r_vertex[triangles[s]];
					var b = r_vertex[triangles[s + 1]];
					var c = r_vertex[triangles[s + 2]];

					t_vertex[s / 3] = (a + b + c) / 3;
				}
			}
		}

		public static int s_next_s(int s) => (s % 3 == 2) ? s - 2 : s + 1;

		public static int s_prev_s(int s) => (s % 3 == 0) ? s + 2 : s - 1;

		public static int s_to_t(int s) => s / 3;

		public bool r_anyghost(int r)
		{
			var out_r = new int[0];
			Circulate(ref out_r, s_end_r, null, r);

			for (int i = 0; i < out_r.Length; i++)
			{
				if (r_ghost(out_r[i]))
				{
					return true;
				}
			}

			return false;
		}

		public int[] r_circulate_r(int r)
		{
			var out_r = new int[0];
			Circulate(ref out_r, s_end_r, r_ghost, r);
			return out_r;
		}

		public int[] r_circulate_s(int r)
		{
			var out_r = new int[0];
			Circulate(ref out_r, null, s_ghost, r);
			return out_r;
		}

		public int[] r_circulate_t(int r)
		{
			var out_t = new int[0];
			Circulate(ref out_t, s_to_t, t_ghost, r);
			return out_t;
		}

		public int[] r_circulate_t_unchecked(int r)
		{
			var out_t = new int[0];
			Circulate(ref out_t, s_to_t, null, r);
			return out_t;
		}

		public bool r_ghost(int r) => r == numRegions;

		public Vector2 r_pos(int r) => r_vertex[r];

		public int s_begin_r(int s)
		{
			return triangles[s];
		}

		public int s_end_r(int s)
		{
			return triangles[s_next_s(s)];
		}

		public bool s_ghost(int s) => s >= numSolidSides;

		public int s_inner_t(int s)
		{
			return s_to_t(s);
		}

		public int s_opposite_s(int s)
		{
			return halfedges[s];
		}

		public int s_outer_t(int s)
		{
			return s_to_t(halfedges[s]);
		}

		public int[] t_circulate_r(int t)
		{
			var out_r = new int[0];
			for (int i = 0; i < 3; i++)
			{
				var v = 3 * t + i;
				if (!s_ghost(v))
				{
					Push(ref out_r, triangles[v]);
				}
			}
			return out_r;
		}

		public int[] t_circulate_s(int t)
		{
			var out_s = new int[3];
			for (int i = 0; i < 3; i++)
			{
				var v = 3 * t + i;
				if (!s_ghost(v))
				{
					out_s[i] = v;
				}
			}
			return out_s;
		}

		public int[] t_circulate_t(int t)
		{
			var out_t = new int[3];
			for (int i = 0; i < 3; i++)
			{
				var v = s_outer_t(3 * t + i);
				if (!t_ghost(v))
				{
					out_t[i] = v;
				}
			}
			return out_t;
		}

		public bool t_ghost(int t) => s_ghost(3 * t);

		public Vector2 t_pos(int t) => t_vertex[t];

		private void Circulate(ref int[] array, Func<int, int> selector, Func<int, bool> ghost, int r)
		{
			var s0 = r_any_s[r];
			var s = s0;
			do
			{
				var v = selector?.Invoke(s) ?? s;
				if (!ghost?.Invoke(v) ?? true)
				{
					Push(ref array, v);
				}
				s = s_next_s(halfedges[s]);
			} while (s != s0);
		}
	}
}
