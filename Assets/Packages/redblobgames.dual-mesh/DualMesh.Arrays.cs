using System;

namespace redblobgames
{
	public partial class DualMesh
	{
		private static void Add<T>(ref T[] array, T value) => Splice(ref array, array.Length, value);

		private static void Push<T>(ref T[] array, T value) => Splice(ref array, array.Length, value);

		private static void Reverse<T>(ref T[] array, int start, int end) => Array.Reverse(array, start, end - start);

		private static T Set<T>(ref T[] array, int index, T value)
		{
			if (index >= array.Length)
			{
				Array.Resize(ref array, index + 1);
			}
			array[index] = value;

			return value;
		}

		private static void Splice<T>(ref T[] array, int index, T value)
		{
			var count = array.Length - index;
			Array.Resize(ref array, array.Length + 1);
			Array.Copy(array, index, array, index + 1, count);
			array[index] = value;
		}

		private static void Unshift<T>(ref T[] array, T value) => Splice(ref array, 0, value);
	}
}
