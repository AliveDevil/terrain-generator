using TerrainGenerator.Data;
using UnityEngine;

namespace TerrainGenerator.Structures
{
	public class StructureInstance : MonoBehaviour
	{
		public Structure Structure { get; set; }
	}
}
