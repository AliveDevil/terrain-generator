using TerrainGenerator.Contracts;

namespace TerrainGenerator
{
	public abstract class TileProvider : StateBehaviour, ITileProvider
	{
		private IMeasure measure;
		private ITerrainController terrainController;

		public IMeasure Measure => measure ?? (measure = TerrainState.Measure);

		public ITerrainController TerrainController => terrainController ?? (terrainController = TerrainState.TerrainController);

		public void Elevate(TerrainSample sample)
		{
			using (Measure.Measure("Tile Provider Elevate"))
			{
				OnElevate(sample);
			}
		}

		public void Sample(TerrainSample sample)
		{
			using (Measure.Measure("Tile Provider Sample"))
			{
				OnSample(sample);
			}
		}

		protected abstract void OnElevate(TerrainSample sample);

		protected sealed override void OnInitialize()
		{
			using (Measure.Measure("Tile Provider Initialize"))
			{
				OnInitializeTileProvider();
			}
		}

		protected abstract void OnInitializeTileProvider();

		protected sealed override void OnRegister()
		{
			TerrainState.TileProvider = this;
			OnRegisterTileProvider();
		}

		protected abstract void OnRegisterTileProvider();

		protected abstract void OnSample(TerrainSample sample);

		protected sealed override void OnSetup()
		{
			using (Measure.Measure("Tile Provider Setup"))
			{
				OnSetupTileProvider();
			}
		}

		protected abstract void OnSetupTileProvider();

		protected sealed override void OnTearDown()
		{
		}
	}
}
