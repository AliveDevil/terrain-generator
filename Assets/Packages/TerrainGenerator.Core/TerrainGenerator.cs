using System;
using System.Threading.Tasks;
using TerrainGenerator.Contracts;
using TerrainGenerator.Data.Assets;
using UnityEngine;

namespace TerrainGenerator.Core
{
	public class TerrainGenerator : StateBehaviour, ITerrainGenerator
	{
		private IBiomeGraph biomeGraph;
		private TaskFactory taskFactory;
		private ITileProvider tileProvider;

		public IBiomeGraph BiomeGraph => biomeGraph ?? (biomeGraph = TerrainState.BiomeGraph);

		public TaskFactory TaskFactory => taskFactory ?? (taskFactory = TerrainState.TaskFactory);

		public ITileProvider TileProvider => tileProvider ?? (tileProvider = TerrainState.TileProvider);

		public void Generate(Vector2Int chunkIndex, Action<Vector2Int, ITile> callback) => ChunkGenerator(chunkIndex, callback);

		public Task GenerateAsync(Vector2Int chunkIndex, Action<Vector2Int, ITile> callback) => TaskFactory.StartNew(() => ChunkGenerator(chunkIndex, callback));

		protected override void OnInitialize()
		{
		}

		protected override void OnRegister()
		{
			TerrainState.TerrainGenerator = this;
		}

		protected override void OnSetup()
		{
		}

		protected override void OnTearDown()
		{
		}

		private void ChunkGenerator(Vector2Int chunkIndex, Action<Vector2Int, ITile> callback)
		{
			using (TerrainState.Measure.Measure("Generate Chunk"))
			{
				var chunkSize = TerrainState.TerrainController.ChunkSize;
				for (int x = 0; x < chunkSize; x++)
				{
					for (int y = 0; y < chunkSize; y++)
					{
						Vector2Int v = new Vector2Int(x, y);

						var sample = new TerrainSample(chunkIndex * chunkSize + v);
						TileProvider.Sample(sample);
						sample.Biome = GetBiome(sample);
						TileProvider.Elevate(sample);

						callback(v, GetTile(sample));
					}
				}
			}
		}

		private BiomeAsset GetBiome(TerrainSample sample)
		{
			if (!sample.Surface)
			{
				return BiomeGraph.Ocean;
			}
			return BiomeGraph.Find(sample.Temperature, sample.Rainfall);
		}

		private Tile GetTile(TerrainSample sample)
		{
			if (sample.Surface)
			{
				return new LandTile
				{
					Biome = sample.Biome,
					Height = sample.Elevation
				};
			}
			else
			{
				return new WaterTile
				{
					Biome = BiomeGraph.Ocean,
					Depth = sample.Elevation
				};
			}
		}
	}
}
