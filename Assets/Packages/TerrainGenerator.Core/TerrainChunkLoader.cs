namespace TerrainGenerator.Core
{
	public class TerrainChunkLoader : StateBehaviour
	{
		protected override void OnInitialize()
		{
		}

		protected override void OnRegister()
		{
		}

		protected override void OnSetup()
		{
			TerrainState.TerrainTransform = transform;
		}

		protected override void OnTearDown()
		{
		}
	}
}
