using System;

namespace TerrainGenerator
{
	[Flags]
	public enum FaceDirection
	{
		Top = 1, Bottom = 2,
		North = 4, West = 8, South = 16, East = 32
	}
}
