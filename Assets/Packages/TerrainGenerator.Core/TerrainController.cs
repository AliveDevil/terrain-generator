using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TerrainGenerator.Contracts;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace TerrainGenerator.Core
{
	public partial class TerrainController : StateBehaviour, ITerrainController
	{
		private Task chunkLoaderTask;
		[SerializeField]
		private TerrainChunk chunkPrefab = null;
		[SerializeField]
		private int chunkSize = 16;
		[SerializeField]
		private GameObject fluidChunkPartPrefab = null;
		private RectInt size;
		[SerializeField]
		private GameObject solidChunkPartPrefab = null;
		private IStructureGenerator structureGenerator;
		private Vector2Int[] surrounds;
		private GameObject tempGameObject;

		public int ChunkSize => chunkSize;

		public RectInt Size
		{
			get { return size; }
			set { size = value; }
		}

		public IStructureGenerator StructureGenerator => structureGenerator ?? (structureGenerator = TerrainState.StructureGenerator);

		protected Dictionary<Vector2Int, TerrainChunk> Chunks { get; } = new Dictionary<Vector2Int, TerrainChunk>();

		public TerrainChunk AllocateChunk(Vector2Int index)
		{
			TerrainChunk chunk;
			if (!TryGetChunk(out chunk))
			{
				chunk = CreateNewChunk();
			}
			chunk.gameObject.name = string.Format("Chunk {0}-{1}", index.x, index.y);
			chunk.Index = index;

			Chunks[index] = chunk;

			return chunk;
		}

		public void DestroyChunk(TerrainChunk chunk)
		{
			Chunks.Remove(chunk.Index);
			Destroy(chunk.gameObject);
		}

		public Vector2Int GetChunkIndex(Vector2 v) => new Vector2Int(Mathf.FloorToInt(v.x / ChunkSize), Mathf.FloorToInt(v.y / ChunkSize));

		public Vector2Int GetChunkIndex(Vector2Int v) => new Vector2Int(FastFloorDiv(v.x, ChunkSize), FastFloorDiv(v.y, ChunkSize));

		public Vector2Int GetChunkIndex(Vector3 v) => new Vector2Int(Mathf.FloorToInt(v.x / ChunkSize), Mathf.FloorToInt(v.z / ChunkSize));

		public Vector2Int GetTileIndex(Vector2 v) => new Vector2Int(Mathf.RoundToInt((v.x % ChunkSize + ChunkSize) % ChunkSize), Mathf.RoundToInt((v.y % ChunkSize + ChunkSize) % ChunkSize));

		public Vector2Int GetTileIndex(Vector2Int v) => new Vector2Int((v.x % ChunkSize + ChunkSize) % ChunkSize, (v.y % ChunkSize + ChunkSize) % ChunkSize);

		public Vector2Int GetTileIndex(Vector3 v) => new Vector2Int(Mathf.RoundToInt((v.x % ChunkSize + ChunkSize) % ChunkSize), Mathf.RoundToInt((v.z % ChunkSize + ChunkSize) % ChunkSize));

		public Vector2 GetTilePosition(Vector2 v) => new Vector2(Mathf.Round(v.x), Mathf.Round(v.y));

		public Vector2 GetTilePosition(Vector3 v) => new Vector2(Mathf.Round(v.x), Mathf.Round(v.z));

		public Task LoadChunk(Vector2Int chunkIndex) => LoadChunk(chunkIndex, CancellationToken.None);

		public async Task LoadChunk(Vector2Int chunkIndex, CancellationToken token)
		{
			var chunk = await MainThreadTaskFactory.StartNew(() => AllocateChunk(chunkIndex), token).LogExceptions();
			chunk.BeginChange();
			await TerrainGenerator.GenerateAsync(chunk.Index, chunk.SetTile).LogExceptions();
			chunk.EndChange();
			StructureGenerator.Generate(chunk, chunkIndex, ChunkSize, chunk.GetTile, TerrainState.Seed ^ chunkIndex.GetHashCode());
		}

		public void Run()
		{
			chunkLoaderTask = ChunkLoaderAsync(TerrainState.CancellationToken);
		}

		public ITile Sample(Vector2Int position)
		{
			var chunkIndex = GetChunkIndex(position);
			var innerBlock = new Vector2Int((position.x % ChunkSize + ChunkSize) % ChunkSize, (position.y % ChunkSize + ChunkSize) % ChunkSize);

			TerrainChunk chunk;
			if (Chunks.TryGetValue(chunkIndex, out chunk))
			{
				return chunk.GetTile(innerBlock);
			}
			return null;
		}

		protected override void OnInitialize()
		{
			Size = TerrainState.Size;
		}

		protected override void OnRegister()
		{
			TerrainState.TerrainObject = gameObject;
			TerrainState.TerrainController = this;
		}

		protected override void OnSetup()
		{
			tempGameObject = new GameObject() { hideFlags = HideFlags.HideAndDontSave };
			SceneManager.MoveGameObjectToScene(tempGameObject, gameObject.scene);
			tempGameObject.transform.SetParent(transform, false);
			tempGameObject.SetActive(false);

			const int radius = 5;
			const int radiusIncreased = radius * 2 + 1;
			const int length = radiusIncreased * radiusIncreased;
			surrounds = Enumerable.Range(0, length).AsParallel().Select(
				x => new Vector2Int(x % radiusIncreased - radius, x / radiusIncreased % radiusIncreased - radius)).OrderBy(x => x.sqrMagnitude).ToArray();
		}

		protected override void OnTearDown()
		{
			Destroy(tempGameObject);
		}

		private static int FastFloorDiv(int a, int b)
		{
			int d = a / b;
			int r = a % b;  /* optimizes into single division. */
			return r != 0 ? (d - ((a < 0) ^ (b < 0) ? 1 : 0)) : d;
		}

		private async Task ChunkLoaderAsync(CancellationToken token)
		{
			HashSet<Vector2Int> testedChunks = new HashSet<Vector2Int>();
			int emptyLoadAttempts = -1;
			while (!token.IsCancellationRequested)
			{
				token.ThrowIfCancellationRequested();
				emptyLoadAttempts += 1;

				var reference = await MainThreadTaskFactory.StartNew(() => GetChunkIndex(TerrainTransform.position)).LogExceptions();

				var nearestChunk = await FindNearestChunk(reference, testedChunks, surrounds).LogExceptions();
				testedChunks.Clear();
				if (nearestChunk.HasValue)
				{
					emptyLoadAttempts = 0;
					await LoadChunk(nearestChunk.Value, token).LogExceptions();
				}
				else
				{
					await Task.Delay(Mathf.Clamp(15 * emptyLoadAttempts / 2, 0, 60), token);
				}
			}
		}

		private TerrainChunk CreateNewChunk()
		{
			var chunk = Instantiate(chunkPrefab, tempGameObject.transform, false);
			chunk.gameObject.SetActive(false);
			chunk.transform.SetParent(transform, false);

			chunk.gameObject.SetActive(true);

			return chunk;
		}

		private Task<Vector2Int?> FindNearestChunk(Vector2Int reference, HashSet<Vector2Int> testedChunks)
		{
			return TaskFactory.StartNew<Vector2Int?>(() =>
			{
				if (!testedChunks.Contains(reference))
				{
					testedChunks.Add(reference);
					if (!OutOfRange(reference))
					{
						if (!Chunks.ContainsKey(reference))
						{
							return reference;
						}
					}
				}
				for (int d = 1; d < 10; d++)
				{
					var north = reference + new Vector2Int(0, d);
					var south = reference + new Vector2Int(0, -d);
					var east = reference + new Vector2Int(d, 0);
					var west = reference + new Vector2Int(-d, 0);
					if (!testedChunks.Contains(north))
					{
						testedChunks.Add(north);
						if (!OutOfRange(north))
						{
							if (!Chunks.ContainsKey(north))
							{
								return north;
							}
						}
					}
					if (!testedChunks.Contains(east))
					{
						testedChunks.Add(east);
						if (!OutOfRange(east))
						{
							if (!Chunks.ContainsKey(east))
							{
								return east;
							}
						}
					}
					if (!testedChunks.Contains(west))
					{
						testedChunks.Add(west);
						if (!OutOfRange(west))
						{
							if (!Chunks.ContainsKey(west))
							{
								return west;
							}
						}
					}
					if (!testedChunks.Contains(south))
					{
						testedChunks.Add(south);
						if (!OutOfRange(south))
						{
							if (!Chunks.ContainsKey(south))
							{
								return south;
							}
						}
					}

					for (int p = 1; p < d + 1; p++)
					{
						var northRight = north + new Vector2Int(p, 0);
						var northLeft = north + new Vector2Int(-p, 0);
						var southRight = south + new Vector2Int(p, 0);
						var southLeft = south + new Vector2Int(-p, 0);
						var westTop = west + new Vector2Int(0, p);
						var westBottom = west + new Vector2Int(0, -p);
						var eastTop = east + new Vector2Int(0, p);
						var eastBottom = east + new Vector2Int(0, -p);

						if (!testedChunks.Contains(northRight))
						{
							testedChunks.Add(northRight);
							if (!OutOfRange(northRight))
							{
								if (!Chunks.ContainsKey(northRight))
								{
									return northRight;
								}
							}
						}
						if (!testedChunks.Contains(northLeft))
						{
							testedChunks.Add(northLeft);
							if (!OutOfRange(northLeft))
							{
								if (!Chunks.ContainsKey(northLeft))
								{
									return northLeft;
								}
							}
						}
						if (!testedChunks.Contains(southRight))
						{
							testedChunks.Add(southRight);
							if (!OutOfRange(southRight))
							{
								if (!Chunks.ContainsKey(southRight))
								{
									return southRight;
								}
							}
						}
						if (!testedChunks.Contains(southLeft))
						{
							testedChunks.Add(southLeft);
							if (!OutOfRange(southLeft))
							{
								if (!Chunks.ContainsKey(southLeft))
								{
									return southLeft;
								}
							}
						}
						if (!testedChunks.Contains(westTop))
						{
							testedChunks.Add(westTop);
							if (!OutOfRange(westTop))
							{
								if (!Chunks.ContainsKey(westTop))
								{
									return westTop;
								}
							}
						}
						if (!testedChunks.Contains(westBottom))
						{
							testedChunks.Add(westBottom);
							if (!OutOfRange(westBottom))
							{
								if (!Chunks.ContainsKey(westBottom))
								{
									return westBottom;
								}
							}
						}
						if (!testedChunks.Contains(eastTop))
						{
							testedChunks.Add(eastTop);
							if (!OutOfRange(eastTop))
							{
								if (!Chunks.ContainsKey(eastTop))
								{
									return eastTop;
								}
							}
						}
						if (!testedChunks.Contains(eastBottom))
						{
							testedChunks.Add(eastBottom);
							if (!OutOfRange(eastBottom))
							{
								if (!Chunks.ContainsKey(eastBottom))
								{
									return eastBottom;
								}
							}
						}
					}
				}
				return null;
			});
		}

		private Task<Vector2Int?> FindNearestChunk(Vector2Int reference, HashSet<Vector2Int> testedChunks, Vector2Int[] surrounds)
		{
			return TaskFactory.StartNew<Vector2Int?>(() =>
			{
				for (int i = 0, n = surrounds.Length; i < n; i++)
				{
					var d = surrounds[i];
					var world = reference + d;
					if (testedChunks.Contains(world))
					{
						continue;
					}
					testedChunks.Add(world);
					if (OutOfRange(world))
					{
						continue;
					}
					if (Chunks.ContainsKey(world))
					{
						continue;
					}
					return world;
				}
				return null;
			});
		}

		private bool OutOfRange(Vector2Int chunk)
		{
			if (chunk.x < size.xMin || chunk.x > size.xMax)
				return true;
			if (chunk.y < size.yMin || chunk.y > size.yMax)
				return true;
			return false;
		}

		private bool TryGetChunk(out TerrainChunk chunk)
		{
			chunk = null;
			return false;
		}
	}
}
