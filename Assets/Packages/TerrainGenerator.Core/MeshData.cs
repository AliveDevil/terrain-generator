using System.Collections.Generic;
using TerrainGenerator.Contracts;
using UnityEngine;

namespace TerrainGenerator.Terrain
{
	public class MeshData : IMeshData
	{
		private readonly List<int> collisionTriangles = new List<int>();
		private readonly List<Vector3> collisionVertices = new List<Vector3>();
		private readonly List<Color> colors = new List<Color>();
		private readonly List<int> triangles = new List<int>();
		private readonly List<Vector3> vertices = new List<Vector3>();

		public void AddQuad(bool collision, bool sync)
		{
			if (sync)
			{
				AddQuad(false, false);
				AddQuad(true, false);
			}
			else
			{
				List<Vector3> vertices;
				List<int> triangles;

				if (collision)
				{
					vertices = collisionVertices;
					triangles = collisionTriangles;
				}
				else
				{
					vertices = this.vertices;
					triangles = this.triangles;
				}

				triangles.Add(vertices.Count - 4); // 0
				triangles.Add(vertices.Count - 3); // 1
				triangles.Add(vertices.Count - 2); // 2

				triangles.Add(vertices.Count - 2); // 2
				triangles.Add(vertices.Count - 1); // 3
				triangles.Add(vertices.Count - 4); // 0
			}
		}

		public void AddQuadColor(Color color)
		{
			colors.Add(color);
			colors.Add(color);
			colors.Add(color);
			colors.Add(color);
		}

		public void AddVertex(Vector3 vertex, bool collision, bool sync)
		{
			if (sync)
			{
				AddVertex(vertex, false, false);
				AddVertex(vertex, true, false);
			}
			else
			{
				if (collision)
				{
					collisionVertices.Add(vertex);
				}
				else
				{
					vertices.Add(vertex);
				}
			}
		}

		public void Begin()
		{
			vertices.Clear();
			triangles.Clear();
			colors.Clear();

			collisionVertices.Clear();
			collisionTriangles.Clear();
		}

		public void Fill(Mesh mesh, Mesh collisionMesh)
		{
			mesh.SetVertices(vertices);
			mesh.SetTriangles(triangles, 0);
			mesh.SetColors(colors);

			collisionMesh.SetVertices(collisionVertices);
			collisionMesh.SetTriangles(collisionTriangles, 0);
		}
	}
}
