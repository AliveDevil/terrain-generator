using TerrainGenerator.Contracts;
using TerrainGenerator.Terrain;
using UnityEngine;

namespace TerrainGenerator
{
	public class MeshDataContainer : IMeshDataContainer
	{
		public IMeshData Fluid { get; } = new MeshData();

		public IMeshData Solid { get; } = new MeshData();

		public void Begin()
		{
			Solid.Begin();
			Fluid.Begin();
		}

		public void Fill(Mesh solidMesh, Mesh fluidMesh, Mesh solidCollisionMesh, Mesh fluidCollisionMesh)
		{
			Solid.Fill(solidMesh, solidCollisionMesh);
			Fluid.Fill(fluidMesh, fluidCollisionMesh);
		}
	}
}
