using System;
using System.Collections.Generic;
using TerrainGenerator.Contracts;
using UnityEngine;

namespace TerrainGenerator
{
	public class MeshPool : MonoBehaviour, IMeshPool
	{
		private Queue<Mesh> meshes = new Queue<Mesh>();

		public Mesh Pop()
		{
			if (!isActiveAndEnabled)
			{
				throw new InvalidOperationException();
			}
			if (meshes.Count > 0)
			{
				return meshes.Dequeue();
			}
			return new Mesh();
		}

		public void Push(Mesh mesh)
		{
			if (!mesh)
			{
				return;
			}
			if (!isActiveAndEnabled)
			{
				DestroyImmediate(mesh);
				return;
			}
			mesh.Clear();
			meshes.Enqueue(mesh);
		}

		private void OnDisable()
		{
			while (meshes.Count > 0)
			{
				DestroyImmediate(meshes.Dequeue());
			}
		}

		private void OnEnable()
		{
			gameObject.GetTerrainState().MeshPool = this;
			for (int i = 0; i < 9 * 4; i++)
			{
				meshes.Enqueue(new Mesh());
			}
		}
	}
}
