using System.Threading.Tasks;
using TerrainGenerator.Contracts;
using UnityEngine;

namespace TerrainGenerator.Core
{
	public partial class TerrainController
	{
		private IBiomeGraph biomeGraph;
		private TaskFactory generatorBackgroundTaskFactory;
		private TaskFactory mainThreadTaskFactory;
		private IMeasure measure;
		private IMeshGenerator meshGenerator;
		private TaskFactory taskFactory;
		private ITerrainGenerator terrainGenerator;
		private Transform terrainTransform;
		private ITileProvider tileProvider;

		public IBiomeGraph BiomeGraph => biomeGraph ?? (biomeGraph = TerrainState.BiomeGraph);

		public TaskFactory GeneratorBackgroundTaskFactory => generatorBackgroundTaskFactory ?? (generatorBackgroundTaskFactory = TerrainState.GeneratorBackgroundTaskFactory);

		public TaskFactory MainThreadTaskFactory => mainThreadTaskFactory ?? (mainThreadTaskFactory = TerrainState.MainThreadTaskFactory);

		public IMeasure Measure => measure ?? (measure = TerrainState.Measure);

		public IMeshGenerator MeshGenerator => meshGenerator ?? (meshGenerator = TerrainState.MeshGenerator);

		public TaskFactory TaskFactory => taskFactory ?? (taskFactory = TerrainState.TaskFactory);

		public ITerrainGenerator TerrainGenerator => terrainGenerator ?? (terrainGenerator = TerrainState.TerrainGenerator);

		public Transform TerrainTransform => terrainTransform ?? (terrainTransform = TerrainState.TerrainTransform);

		public ITileProvider TileProvider => tileProvider ?? (tileProvider = TerrainState.TileProvider);
	}
}
