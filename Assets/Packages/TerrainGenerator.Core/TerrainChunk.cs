using FieldTreeStructure;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using TerrainGenerator.Contracts;
using TerrainGenerator.Data;
using TerrainGenerator.Data.Assets;
using TerrainGenerator.Structures;
using UnityEngine;

namespace TerrainGenerator
{
	public class TerrainChunk : MonoBehaviour, IStructureContainer
	{
		private CancellationTokenSource activeRenderCancellationToken;
		private HashSet<Vector2Int> changedTiles = new HashSet<Vector2Int>();
		private CancellationTokenSource chunkCancellationTokenSource;
		private ManualResetEventSlim deferChangeEvent = new ManualResetEventSlim(true);
		private bool deferRequireUpdate = false;
		[SerializeField]
		private MeshCollider fluidMeshCollider = null;
		[SerializeField]
		private MeshFilter fluidMeshFilter = null;
		[SerializeField]
		private MeshRenderer fluidMeshRenderer = null;
		[SerializeField]
		private Vector2Int index;
		private IMeshPool meshPool;
		[SerializeField]
		private bool requireUpdate = false;
		[SerializeField]
		private MeshCollider solidMeshCollider = null;
		[SerializeField]
		private MeshFilter solidMeshFilter = null;
		[SerializeField]
		private MeshRenderer solidMeshRenderer = null;
		private TerrainState terrainState;
		private ITile[,] tiles;
		private CoverFieldTree<Structure> tree;

		public CancellationToken CancellationToken => chunkCancellationTokenSource.Token;

		public Vector2Int Index
		{
			get
			{
				return index;
			}
			set
			{
				index = value;
				transform.localPosition = new Vector3(index.x, 0, index.y) * TerrainController.ChunkSize;
			}
		}

		public bool RequireUpdate => requireUpdate;

		public TerrainState TerrainState => terrainState ?? (terrainState = gameObject.GetTerrainState());

		public CoverFieldTree<Structure> Tree => tree;

		public TaskFactory GeneratorBackgroundTaskFactory => terrainState.GeneratorBackgroundTaskFactory;

		public TaskFactory MainThreadTaskFactory => terrainState.MainThreadTaskFactory;

		public IMeshGenerator MeshGenerator => terrainState.MeshGenerator;

		public IMeshPool MeshPool => meshPool ?? (meshPool = TerrainState.MeshPool);

		public TaskFactory PhysicsTaskFactory => terrainState.PhysicsTaskFactory;

		public TaskFactory TaskFactory => terrainState.TaskFactory;

		public ITerrainController TerrainController => terrainState.TerrainController;

		public void BeginChange()
		{
			deferChangeEvent.Reset();
		}

		public void Change()
		{
			if (deferChangeEvent.Wait(0))
			{
				requireUpdate = true;
				Render();
			}
			else
			{
				deferRequireUpdate = true;
			}
		}

		public void Change(Vector2Int tile)
		{
			Change();
			if (deferChangeEvent.Wait(0))
			{
			}
			else
			{
				changedTiles.Add(tile);
			}
		}

		public void EndChange()
		{
			deferChangeEvent.Set();
			requireUpdate |= deferRequireUpdate;
			deferRequireUpdate = false;
			foreach (var item in changedTiles)
			{
			}
			changedTiles.Clear();
			Render();
		}

		public ITile GetTile(Vector2Int v)
		{
			EnsureTilesCreated();
			if (v.x < 0 || v.y < 0 || v.x >= TerrainController.ChunkSize || v.y >= TerrainController.ChunkSize)
				return null;
			return tiles[v.x, v.y];
		}

		public IEnumerator<TileThingy> GetTiles()
		{
			return new ChunkEnumerator(this);
		}

		public void Render()
		{
			if (activeRenderCancellationToken != null)
			{
				activeRenderCancellationToken.Cancel();
				activeRenderCancellationToken.Dispose();
			}
			activeRenderCancellationToken = CancellationTokenSource.CreateLinkedTokenSource(CancellationToken);
			requireUpdate = false;
			RenderAsync(activeRenderCancellationToken.Token).LogExceptions();
		}

		public async Task RenderAsync(CancellationToken token)
		{
			token.ThrowIfCancellationRequested();
			IMeshDataContainer meshData = new MeshDataContainer();
			await MeshGenerator.GenerateMeshAsync(meshData, new ChunkEnumerator(this)).LogExceptions();

			Mesh solidMesh = null;
			Mesh solidCollisionMesh = null;
			Mesh fluidMesh = null;
			Mesh fluidCollisionMesh = null;

			await MainThreadTaskFactory.StartNew(() =>
			{
				solidMesh = MeshPool.Pop();
				solidCollisionMesh = MeshPool.Pop();
				fluidMesh = MeshPool.Pop();
				fluidCollisionMesh = MeshPool.Pop();
			}, token).LogExceptions();

			await GeneratorBackgroundTaskFactory.StartNew(() =>
			{
				meshData.Fill(solidMesh, fluidMesh, solidCollisionMesh, fluidCollisionMesh);
			}, token).LogExceptions();

			await MainThreadTaskFactory.StartNew(() =>
			{
				solidMesh.RecalculateNormals();
				solidCollisionMesh.RecalculateNormals();
				solidCollisionMesh.RecalculateBounds();
			}, token).LogExceptions();
			await MainThreadTaskFactory.StartNew(() =>
			{
				fluidMesh.RecalculateNormals();
				fluidCollisionMesh.RecalculateNormals();
				fluidCollisionMesh.RecalculateBounds();
			}, token).LogExceptions();

			var renderTask = MainThreadTaskFactory.StartNew(() =>
			{
				var previousSolid = solidMeshFilter.sharedMesh;
				var previousFluid = fluidMeshFilter.sharedMesh;

				solidMeshFilter.sharedMesh = solidMesh;
				fluidMeshFilter.sharedMesh = fluidMesh;

				MeshPool.Push(previousFluid);
				MeshPool.Push(previousSolid);
			}).LogExceptions();
			var collisionTask = PhysicsTaskFactory.StartNew(() =>
			{
				var previousSolid = solidMeshCollider.sharedMesh;
				var previousFluid = fluidMeshCollider.sharedMesh;

				solidMeshCollider.sharedMesh = solidCollisionMesh;
				fluidMeshCollider.sharedMesh = fluidCollisionMesh;

				MeshPool.Push(fluidCollisionMesh);
				MeshPool.Push(solidCollisionMesh);
			}).LogExceptions();
			await Task.WhenAll(renderTask, collisionTask);
		}

		public void SetTile(Vector2Int v, ITile tile)
		{
			EnsureTilesCreated();
			tiles[v.x, v.y] = tile;
			// Append additional information (Neighbors etc.)
			Change(v);
		}

		void IStructureContainer.Spawn(Vector2Int position, Vector2Int size, float rotation, StructureAsset structureAsset, GameObject prefab)
		{
			GeneratorBackgroundTaskFactory.StartNew(() =>
			{
				var instance = Instantiate(prefab, gameObject.transform, false);
				var transform = instance.transform;
				transform.localPosition = new Vector3(position.x, GetTile(position).GetHeight(), position.y);
				transform.localRotation = Quaternion.Euler(0, rotation, 0);
				tree.Add(Structure.Build().WithAsset(structureAsset).WithInstance(instance).WithPosition(position).WithSize(size).Build());
			});
		}

		IStructureSpawn IStructureContainer.Spawn() => new StructureSpawner(this);

		private void EnsureTilesCreated()
		{
			if (tiles != null)
			{
				return;
			}

			tiles = new Tile[TerrainController.ChunkSize, TerrainController.ChunkSize];
		}

		private ITile LookupTile(Vector2Int v)
		{
			var chunkIndex = TerrainController.GetChunkIndex(v) + Index;
			var tileIndex = TerrainController.GetTileIndex(v);

			TerrainChunk chunk = this;
			if (chunkIndex != Index)
			{
				return null; // Out of Bounds!
			}
			return chunk.GetTile(tileIndex);
		}

		private void OnDisable()
		{
			chunkCancellationTokenSource.Cancel();
			chunkCancellationTokenSource.Dispose();
		}

		private void OnEnable()
		{
			terrainState = TerrainState.PerScene(gameObject);
			chunkCancellationTokenSource = new CancellationTokenSource();

			tree = new CoverFieldTree<Structure>(
				TerrainController.ChunkSize, TerrainController.ChunkSize, 1,
				TerrainController.ChunkSize / 2, TerrainController.ChunkSize / 2);
		}

		private struct ChunkEnumerator : IEnumerator<TileThingy>
		{
			private TerrainChunk chunk;
			private int chunkSize;
			private int index;
			private int length;

			public TileThingy Current { get; private set; }

			object IEnumerator.Current { get { return Current; } }

			public ChunkEnumerator(TerrainChunk chunk)
			{
				this.chunk = chunk;
				Current = null;
				index = -1;
				chunkSize = chunk.TerrainController.ChunkSize;
				length = chunkSize * chunkSize;
			}

			public void Dispose()
			{
				chunk = null;
				Current = null;
			}

			public bool MoveNext()
			{
				if (chunk == null)
				{
					return false;
				}
				while (index < length)
				{
					index += 1;
					if (index >= length)
					{
						return false;
					}
					int x = index % chunkSize;
					int z = index / chunkSize % chunkSize;
					var v = new Vector2Int(x, z);

					if (chunk.GetTile(v) != null)
					{
						Current = new TileThingy(chunk.GetTile(v), v, chunk);
						return true;
					}
				}
				return false;
			}

			public void Reset()
			{
				index = -1;
				Current = null;
			}
		}

		private class StructureSpawner : IStructureSpawn
		{
			private readonly TerrainChunk chunk;
			private Vector2Int position;
			private GameObject prefab;
			private float rotation;
			private Vector2Int size;
			private StructureAsset structureAsset;

			public StructureSpawner(TerrainChunk chunk)
			{
				this.chunk = chunk;
			}

			void IStructureSpawn.Spawn()
			{
				chunk.MainThreadTaskFactory.StartNew(() =>
				{
					var instance = Instantiate(prefab, chunk.transform, false);
					var transform = instance.transform;
					transform.localPosition = new Vector3(position.x, chunk.GetTile(position).GetHeight(), position.y);
					transform.localRotation = Quaternion.Euler(0, rotation, 0);
					var builder = Structure.Build();
					builder.WithAsset(structureAsset);
					builder.WithPosition(position);
					builder.WithSize(size);
					builder.WithInstance(instance);
					var structure = builder.Build();
					var structureInstance = instance.AddComponent<StructureInstance>();
					structureInstance.Structure = structure;
					chunk.Tree.Add(structure);

					var boxCollider = new GameObject();
					boxCollider.transform.SetParent(transform, false);
					boxCollider.transform.localPosition = new Vector3(0, transform.position.z - transform.localPosition.y, 0);
					var collider = boxCollider.AddComponent<BoxCollider2D>();
					collider.size = new Vector2(size.x, size.y);
				});
			}

			IStructureSpawn IStructureSpawn.WithPosition(Vector2Int position)
			{
				this.position = position;
				return this;
			}

			IStructureSpawn IStructureSpawn.WithPrefab(GameObject prefab)
			{
				this.prefab = prefab;
				return this;
			}

			IStructureSpawn IStructureSpawn.WithRotation(float rotation)
			{
				this.rotation = rotation;
				return this;
			}

			IStructureSpawn IStructureSpawn.WithSize(Vector2Int size)
			{
				this.size = size;
				return this;
			}

			IStructureSpawn IStructureSpawn.WithStructure(StructureAsset asset)
			{
				this.structureAsset = asset;
				return this;
			}
		}
	}
}
