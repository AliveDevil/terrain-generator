namespace TerrainGenerator
{
	public class TileReference
	{
		public Tile Tile { get; set; }

		public T SetTile<T>() where T : Tile, new()
		{
			var t = new T();
			Tile = t;
			return t;
		}
	}
}
