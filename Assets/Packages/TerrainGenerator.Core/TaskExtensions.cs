using System.Threading.Tasks;
using UnityEngine;

namespace TerrainGenerator
{
	public static class TaskExtensions
	{
		public static T LogExceptions<T>(this T task) where T : Task
		{
			task.ContinueWith(Log, TaskContinuationOptions.OnlyOnFaulted);
			return task;
		}

		private static void Log(Task task) => Debug.Log(task.Exception);
	}
}
