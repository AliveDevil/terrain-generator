using TerrainGenerator.Contracts;
using TerrainGenerator.Data.Assets;

namespace TerrainGenerator
{
	public static class TileExtensions
	{
		public static bool Blocked(this ITile reference, ITile compare)
		{
			if (compare == null)
			{
				return false;
			}
			return reference.GetHeight() < compare.GetHeight();
		}

		public static int GetHeight(this ITile tile)
		{
			return GetHeight(tile, true);
		}

		public static int GetHeight(this ITile tile, bool adjustWater)
		{
			if (tile is LandTile)
			{
				var landTile = (LandTile)tile;
				return landTile.Height;
			}
			else if (tile is WaterTile)
			{
				var waterTile = (WaterTile)tile;
				return -(waterTile.Depth + (adjustWater ? 1 : 0));
			}
			else
			{
				return 0;
			}
		}
	}

	public class LandTile : Tile
	{
		public int Height { get; set; }

		public float Rainfall { get; set; }

		public float Temperature { get; set; }
	}

	public abstract class Tile : ITile
	{
		public BiomeAsset Biome { get; set; }
	}

	public class WaterTile : Tile
	{
		public int Depth { get; set; }
	}
}
