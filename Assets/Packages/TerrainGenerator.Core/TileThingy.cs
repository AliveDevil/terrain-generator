using TerrainGenerator.Contracts;
using TerrainGenerator.Core;
using UnityEngine;

namespace TerrainGenerator
{
	public class TileThingy : TileItem
	{
		public TerrainChunk Chunk { get; }

		public TileThingy(ITile tile, Vector2Int location, TerrainChunk chunk) : base(tile, location)
		{
			Chunk = chunk;
		}

		public override ITile GetTile(TileDirection direction)
		{
			switch (direction)
			{
				case TileDirection.North:
					return Chunk.GetTile(Location + Vector2Int.up);

				case TileDirection.South:
					return Chunk.GetTile(Location + Vector2Int.down);

				case TileDirection.East:
					return Chunk.GetTile(Location + Vector2Int.right);

				case TileDirection.West:
					return Chunk.GetTile(Location + Vector2Int.left);

				default:
					return null;
			}
		}
	}
}
