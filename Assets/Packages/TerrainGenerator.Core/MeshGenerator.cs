using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TerrainGenerator.Contracts;
using TerrainGenerator.Core;
using UnityEngine;

namespace TerrainGenerator
{
	public class MeshGenerator : StateBehaviour, IMeshGenerator
	{
		private delegate IEnumerator<TileThingy> GenerateMeshDelegate(IMeshDataContainer meshData, TileItem tile);

		private static GenerateMeshDelegate fluidMesh = GenerateFluidMesh;
		private static GenerateMeshDelegate staticMesh = GenerateStaticMesh;
		private Dictionary<Type, GenerateMeshDelegate> generatorLookup;

		public static IEnumerator<TileThingy> GenerateFluidMesh(IMeshDataContainer meshData, TileItem tile)
		{
			var waterTile = (WaterTile)tile.Tile;

			var faces = EmitFaces(meshData.Fluid, FaceDirection.Top, new Vector3(tile.Location.x, 0, tile.Location.y), false, true);
			EmitFacesColor(meshData.Fluid, Color.blue, faces);

			yield return new TileThingy(new LandTile()
			{
				Height = -(waterTile.Depth + 1),
				Biome = waterTile.Biome
			}, tile.Location, ((TileThingy)tile).Chunk);
		}

		public static IEnumerator<TileThingy> GenerateStaticMesh(IMeshDataContainer meshData, TileItem tile)
		{
			var landTile = (LandTile)tile.Tile;

			int level = 0;
			bool anyLower = true;
			var tileHeight = landTile.Height;

			while (anyLower)
			{
				anyLower = false;
				FaceDirection result = 0;
				var height = tileHeight - level;

				if (level == 0)
				{
					result |= FaceDirection.Top;
				}

				var north = tile.GetTile(TileDirection.North);
				if (!landTile.Blocked(north))
				{
					result |= FaceDirection.North;
					anyLower |= north != null && north.GetHeight(false) < height;
				}
				var west = tile.GetTile(TileDirection.West);
				if (!landTile.Blocked(west))
				{
					result |= FaceDirection.West;
					anyLower |= west != null && west.GetHeight(false) < height;
				}
				var south = tile.GetTile(TileDirection.South);
				if (!landTile.Blocked(south))
				{
					result |= FaceDirection.South;
					anyLower |= south != null && south.GetHeight(false) < height;
				}
				var east = tile.GetTile(TileDirection.East);
				if (!landTile.Blocked(east))
				{
					result |= FaceDirection.East;
					anyLower |= east != null && east.GetHeight(false) < height;
				}
				if (!anyLower)
				{
					result |= FaceDirection.Bottom;
				}

				var faces = EmitFaces(meshData.Solid, result, new Vector3(tile.Location.x, height, tile.Location.y), false, true);
				EmitFacesColor(meshData.Solid, landTile.Biome.Color, faces);

				level++;
			}

			return null;
		}

		public Task GenerateMeshAsync(IMeshDataContainer meshData, IEnumerator<TileItem> enumerator)
		{
			var iterators = new Stack<IEnumerator<TileItem>>(3);
			iterators.Push(enumerator);

			return TerrainState.TaskFactory.StartNew(() =>
			{
				while (iterators.Count > 0)
				{
					var iterator = iterators.Pop();

					if (iterator.MoveNext())
					{
						iterators.Push(iterator);

						var tile = iterator.Current;
						GenerateMeshDelegate generator;
						try
						{
							if (generatorLookup.TryGetValue(tile.Tile.GetType(), out generator))
							{
								var nextIterator = generator(meshData, tile);
								if (nextIterator != null)
								{
									iterators.Push(nextIterator);
								}
							}
						}
						catch (Exception e)
						{
							Debug.LogException(e);
						}
					}
				}
			});
		}

		protected static void EmitFaceBottom(IMeshData meshData, Vector3 v, bool collision, bool sync)
		{
			meshData.AddVertex(new Vector3(v.x - 0.5f, v.y - 0.5f, v.z - 0.5f), collision, sync);
			meshData.AddVertex(new Vector3(v.x + 0.5f, v.y - 0.5f, v.z - 0.5f), collision, sync);
			meshData.AddVertex(new Vector3(v.x + 0.5f, v.y - 0.5f, v.z + 0.5f), collision, sync);
			meshData.AddVertex(new Vector3(v.x - 0.5f, v.y - 0.5f, v.z + 0.5f), collision, sync);

			meshData.AddQuad(collision, sync);
		}

		protected static void EmitFaceEast(IMeshData meshData, Vector3 v, bool collision, bool sync)
		{
			meshData.AddVertex(new Vector3(v.x + 0.5f, v.y - 0.5f, v.z - 0.5f), collision, sync);
			meshData.AddVertex(new Vector3(v.x + 0.5f, v.y + 0.5f, v.z - 0.5f), collision, sync);
			meshData.AddVertex(new Vector3(v.x + 0.5f, v.y + 0.5f, v.z + 0.5f), collision, sync);
			meshData.AddVertex(new Vector3(v.x + 0.5f, v.y - 0.5f, v.z + 0.5f), collision, sync);

			meshData.AddQuad(collision, sync);
		}

		protected static void EmitFaceNorth(IMeshData meshData, Vector3 v, bool collision, bool sync)
		{
			meshData.AddVertex(new Vector3(v.x + 0.5f, v.y - 0.5f, v.z + 0.5f), collision, sync);
			meshData.AddVertex(new Vector3(v.x + 0.5f, v.y + 0.5f, v.z + 0.5f), collision, sync);
			meshData.AddVertex(new Vector3(v.x - 0.5f, v.y + 0.5f, v.z + 0.5f), collision, sync);
			meshData.AddVertex(new Vector3(v.x - 0.5f, v.y - 0.5f, v.z + 0.5f), collision, sync);

			meshData.AddQuad(collision, sync);
		}

		protected static int EmitFaces(IMeshData meshData, FaceDirection faceDirection, Vector3 position, bool collision, bool sync)
		{
			int faces = 0;

			for (int i = 0; i < 6; i++)
			{
				if (((int)faceDirection & (1 << i)) > 0)
				{
					faces++;
					switch ((FaceDirection)(1 << i))
					{
						case FaceDirection.Top:
							EmitFaceTop(meshData, position, collision, sync);
							break;

						case FaceDirection.Bottom:
							EmitFaceBottom(meshData, position, collision, sync);
							break;

						case FaceDirection.North:
							EmitFaceNorth(meshData, position, collision, sync);
							break;

						case FaceDirection.West:
							EmitFaceWest(meshData, position, collision, sync);
							break;

						case FaceDirection.South:
							EmitFaceSouth(meshData, position, collision, sync);
							break;

						case FaceDirection.East:
							EmitFaceEast(meshData, position, collision, sync);
							break;
					}
				}
			}

			return faces;
		}

		protected static void EmitFacesColor(IMeshData meshData, Color color, int faces)
		{
			for (int i = 0; i < faces; i++)
			{
				meshData.AddQuadColor(color);
			}
		}

		protected static void EmitFaceSouth(IMeshData meshData, Vector3 v, bool collision, bool sync)
		{
			meshData.AddVertex(new Vector3(v.x - 0.5f, v.y - 0.5f, v.z - 0.5f), collision, sync);
			meshData.AddVertex(new Vector3(v.x - 0.5f, v.y + 0.5f, v.z - 0.5f), collision, sync);
			meshData.AddVertex(new Vector3(v.x + 0.5f, v.y + 0.5f, v.z - 0.5f), collision, sync);
			meshData.AddVertex(new Vector3(v.x + 0.5f, v.y - 0.5f, v.z - 0.5f), collision, sync);

			meshData.AddQuad(collision, sync);
		}

		protected static void EmitFaceTop(IMeshData meshData, Vector3 v, bool collision, bool sync)
		{
			meshData.AddVertex(new Vector3(v.x - 0.5f, v.y + 0.5f, v.z + 0.5f), collision, sync);
			meshData.AddVertex(new Vector3(v.x + 0.5f, v.y + 0.5f, v.z + 0.5f), collision, sync);
			meshData.AddVertex(new Vector3(v.x + 0.5f, v.y + 0.5f, v.z - 0.5f), collision, sync);
			meshData.AddVertex(new Vector3(v.x - 0.5f, v.y + 0.5f, v.z - 0.5f), collision, sync);

			meshData.AddQuad(collision, sync);
		}

		protected static void EmitFaceWest(IMeshData meshData, Vector3 v, bool collision, bool sync)
		{
			meshData.AddVertex(new Vector3(v.x - 0.5f, v.y - 0.5f, v.z + 0.5f), collision, sync);
			meshData.AddVertex(new Vector3(v.x - 0.5f, v.y + 0.5f, v.z + 0.5f), collision, sync);
			meshData.AddVertex(new Vector3(v.x - 0.5f, v.y + 0.5f, v.z - 0.5f), collision, sync);
			meshData.AddVertex(new Vector3(v.x - 0.5f, v.y - 0.5f, v.z - 0.5f), collision, sync);

			meshData.AddQuad(collision, sync);
		}

		protected override void OnInitialize()
		{
		}

		protected override void OnRegister()
		{
			TerrainState.MeshGenerator = this;
		}

		protected override void OnSetup()
		{
			generatorLookup = new Dictionary<Type, GenerateMeshDelegate>()
			{
				{ typeof(LandTile), staticMesh },
				{ typeof(WaterTile), fluidMesh }
			};
		}

		protected override void OnTearDown()
		{
		}
	}
}
