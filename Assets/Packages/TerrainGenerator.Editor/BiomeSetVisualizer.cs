using d3.delaunay;
using System.Collections.Generic;
using System.IO;
using TerrainGenerator.Data.Assets;
using UnityEditor;
using UnityEngine;

public class BiomeSetVisualizer : EditorWindow
{
	[SerializeField]
	private BiomeSetAsset biomeSet;
	private Delaunay delaunay;
	private bool render = false;
	[SerializeField]
	private int subdivision = 0;
	private Texture2D visualization;
	private Voronoi voronoi;

	[MenuItem("Window/Biome Set Visualizer")]
	public static void OpenWindow()
	{
		GetWindow<BiomeSetVisualizer>().Show(true);
	}

	private void CreateTexture()
	{
		List<float> width = new List<float>() { -1, 1 };
		List<float> height = new List<float>() { 0, 1 };
		for (int i = 0; i < subdivision; i++)
		{
			for (int j = width.Count - 1; j > 0; j--)
			{
				var c = width[j];
				var n = width[j - 1];
				var m = (c + n) / 2;
				width.Insert(j, m);
			}

			for (int j = height.Count - 1; j > 0; j--)
			{
				var c = height[j];
				var n = height[j - 1];
				var m = (c + n) / 2;
				height.Insert(j, m);
			}
		}

		if (visualization)
		{
			DestroyImmediate(visualization);
		}
		visualization = new Texture2D(width.Count, height.Count);

		for (int x = width.Count - 1; x >= 0; x--)
		{
			for (int y = height.Count - 1; y >= 0; y--)
			{
				var temperature = width[x];
				var rainfall = height[y];

				var biomeIndex = voronoi.Find(temperature, rainfall);
				var biome = biomeSet.Biomes[biomeIndex];
				var color = biome.Color;

				visualization.SetPixel(x, y, color);
				visualization.SetPixel(width.Count / 2, y, Color.black);
			}

			visualization.SetPixel(x, 0, Color.black);
		}
		visualization.Apply();
	}

	private void DrawBiomeSelector()
	{
		EditorGUI.BeginChangeCheck();
		biomeSet = (BiomeSetAsset)EditorGUILayout.ObjectField(biomeSet, typeof(BiomeSetAsset), false);
		if (EditorGUI.EndChangeCheck())
		{
			DestroyImmediate(visualization);
			if (biomeSet)
			{
				var biomeConfig = new Vector2[biomeSet.Biomes.Length];
				for (int i = 0, n = biomeConfig.Length; i < n; i++)
				{
					var biome = biomeSet.Biomes[i];
					biomeConfig[i] = new Vector2(biome.MeanTemperature, biome.MeanRainfall);
				}
				delaunay = new Delaunay(biomeConfig);
				voronoi = delaunay.Voronoi(new Rect(-1, 0, 2, 1));
			}
		}
	}

	private void OnDisable()
	{
		DestroyImmediate(visualization);
	}

	private void OnGUI()
	{
		GUILayout.BeginArea(new Rect(Vector2.zero, position.size));

		DrawBiomeSelector();
		Visualization();

		GUILayout.EndArea();
	}

	private void Visualization()
	{
		if (!biomeSet)
		{
			return;
		}

		VisualizationControl();

		if (!visualization)
		{
			CreateTexture();
		}

		if (render)
		{
			render = false;
			File.WriteAllBytes("export.png", visualization.EncodeToPNG());
		}

		Rect rect = GUILayoutUtility.GetAspectRect(1);
		EditorGUI.DrawPreviewTexture(rect, visualization);
	}

	private void VisualizationControl()
	{
		EditorGUI.BeginChangeCheck();
		subdivision = EditorGUILayout.IntSlider("Subdivisions", subdivision, 0, 10);
		GUILayout.BeginHorizontal();
		bool redraw = GUILayout.Button("Redraw");
		bool render = GUILayout.Button("Render");
		GUILayout.EndHorizontal();
		if (EditorGUI.EndChangeCheck() || redraw)
		{
			DestroyImmediate(visualization);
		}
		this.render = render;
	}
}
