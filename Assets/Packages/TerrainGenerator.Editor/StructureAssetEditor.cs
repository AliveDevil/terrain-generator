using TerrainGenerator.Data.Assets;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

[CustomEditor(typeof(StructureAsset)), CanEditMultipleObjects]
public class StructureAssetEditor : BaseEditor
{
	

	[MenuItem("Assets/Create/Terrain Generator/Structures/Procedural")]
	public static void CreateProceduralStructureAsset()
	{
		CreateAsset<ProceduralStructureAsset>("Assets/Resources/Structures", "New Procedural Structure");
	}

	[MenuItem("Assets/Create/Terrain Generator/Structures/Simple")]
	public static void CreateSimpleStructureAsset()
	{
		CreateAsset<SimpleStructureAsset>("Assets/Resources/Structures", "New Simple Structure");
	}
}
