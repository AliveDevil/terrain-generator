using TerrainGenerator.Data.Assets;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

[CustomEditor(typeof(BiomeAsset))]
public class BiomeAssetEditor : BaseEditor
{
	private ReorderableList biomeStructureDefinitionList;
	private SerializedProperty colorProperty;
	private SerializedProperty meanRainfallProperty;
	private SerializedProperty meanTemperatureProperty;
	private SerializedProperty terrainAmplitudeProperty;

	[MenuItem("Assets/Create/Terrain Generator/Biome Asset")]
	public static void CreateBiomeAsset()
	{
		CreateAsset<BiomeSetAsset>("Assets/Resources/Biomes", "New Biome");
	}

	public override void OnInspectorGUI()
	{
		serializedObject.Update();

		EditorGUILayout.PropertyField(colorProperty);
		EditorGUILayout.Slider(meanTemperatureProperty, -1, 1);
		EditorGUILayout.Slider(meanRainfallProperty, 0, 1);
		EditorGUILayout.PropertyField(terrainAmplitudeProperty);
		biomeStructureDefinitionList.DoLayoutList();

		serializedObject.ApplyModifiedProperties();
	}

	private void OnEnable()
	{
		colorProperty = serializedObject.FindProperty("color");
		meanRainfallProperty = serializedObject.FindProperty("meanRainfall");
		meanTemperatureProperty = serializedObject.FindProperty("meanTemperature");
		terrainAmplitudeProperty = serializedObject.FindProperty("terrainAmplitude");

		var property = serializedObject.FindProperty("structureDefinitions");
		biomeStructureDefinitionList = new ReorderableList(serializedObject, property)
		{
			elementHeightCallback = (i) =>
			{
				return (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing) * 3;
			},
			drawHeaderCallback = (rect) =>
			{
				GUI.Label(rect, "Structures");
			},
			drawElementCallback = (rect, index, isActive, isFocused) =>
			{
				var element = property.GetArrayElementAtIndex(index);

				var y = 0f;
				EditorGUI.ObjectField(new Rect(rect.x, rect.y + y, rect.width, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("structure"));
				y += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
				EditorGUI.Slider(new Rect(rect.x, rect.y + y, rect.width, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("chance"), 0, 1);
				y += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
				EditorGUI.PropertyField(new Rect(rect.x, rect.y + y, rect.width, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("densityRadius"));
			}
		};
	}
}
