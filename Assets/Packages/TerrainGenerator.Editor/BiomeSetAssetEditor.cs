using TerrainGenerator.Data.Assets;
using UnityEditor;

[CustomEditor(typeof(BiomeSetAsset))]
public class BiomeSetAssetEditor : BaseEditor
{
	[MenuItem("Assets/Create/Terrain Generator/Biome Set Asset")]
	public static void CreateStructureAsset()
	{
		CreateAsset<BiomeSetAsset>("Assets/Resources", "New Biome Set");
	}
}
