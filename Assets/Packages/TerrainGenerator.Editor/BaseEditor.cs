using UnityEditor;
using UnityEditor.ProjectWindowCallback;
using UnityEngine;

public abstract class BaseEditor : Editor
{
	protected static void CreateAsset<T>(string path, string name) where T : ScriptableObject
	{
		var asset = CreateInstance<T>();
		ProjectWindowUtil.StartNameEditingIfProjectWindowExists(
				asset.GetInstanceID(),
				CreateInstance<EndNameEdit>(),
				$"{path}/{name}.asset",
				AssetPreview.GetMiniThumbnail(asset),
				null);
	}

	internal class EndNameEdit : EndNameEditAction
	{
		public override void Action(int instanceId, string pathName, string resourceFile)
		{
			AssetDatabase.CreateAsset(EditorUtility.InstanceIDToObject(instanceId), AssetDatabase.GenerateUniqueAssetPath(pathName));
		}
	}
}
