using UnityEngine;

namespace d3.delaunay
{
	public partial class Voronoi
	{
		public Vector2[] Circumcenters => circumcenters;

		public int[] Edges => edges;

		public int[] Index => index;

		public float xmax => bounds.xMax;

		public float xmin => bounds.xMin;

		public float ymax => bounds.yMax;

		public float ymin => bounds.yMin;

		public Vector2[] Cell(int i) => this._cell(i);

		public int[] CellRaw(int i) => this._cellraw(i);

		public Vector2[] RenderCell(int i) => this._clip(i);

		private int[] _cellraw(int i)
		{
			var t0 = index[i * 2];
			var t1 = index[i * 2 + 1];
			if (t0 == t1)
				return null;

			var points = new int[t1 - t0];
			for (int t = t0, j = 0; t < t1; t++, j++)
			{
				points[j] = edges[t];
			}
			return points;
		}
	}
}
