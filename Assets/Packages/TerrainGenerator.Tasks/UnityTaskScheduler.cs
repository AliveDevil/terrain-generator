using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TerrainGenerator.Tasks;

public class UnitySingleTaskScheduler : TaskScheduler, IExecutePendingTasks, IDisposable
{
	private readonly LinkedList<Task> queue = new LinkedList<Task>();
	private readonly int synchronousTasks;
	private readonly SynchronizationContext unitySynchronizationContext;
	private bool disposed = false;

	public UnitySingleTaskScheduler(SynchronizationContext context, int synchronousTasks)
	{
		unitySynchronizationContext = context;
		this.synchronousTasks = synchronousTasks;
	}

	public void Dispose()
	{
		disposed = true;
		while (true)
		{
			Task task;
			lock (queue)
			{
				if (queue.Count == 0)
				{
					break;
				}

				task = queue.First.Value;
				queue.RemoveFirst();
			}

			if (task != null)
			{
				task.Dispose();
			}
		}
	}

	public void ExecutePendingTasks()
	{
		for (int i = 0; i < synchronousTasks; i++)
		{
			Task task;
			lock (queue)
			{
				if (queue.Count == 0)
				{
					break;
				}

				task = queue.First.Value;
				queue.RemoveFirst();
			}

			if (task != null)
			{
				var result = TryExecuteTask(task);
				if (result == false)
				{
					throw new InvalidOperationException();
				}
			}
		}
	}

	protected override IEnumerable<Task> GetScheduledTasks()
	{
		lock (queue)
		{
			return queue.ToArray();
		}
	}

	protected override void QueueTask(Task task)
	{
		if (disposed)
		{
			return;
		}
		lock (queue)
		{
			queue.AddLast(task);
		}
	}

	protected override bool TryExecuteTaskInline(Task task, bool taskWasPreviouslyQueued)
	{
		if (SynchronizationContext.Current != unitySynchronizationContext)
		{
			return false;
		}

		if (taskWasPreviouslyQueued)
		{
			lock (queue)
			{
				queue.Remove(task);
			}
		}

		if (disposed)
		{
			return false;
		}

		return TryExecuteTask(task);
	}
}

public class UnityTaskScheduler : TaskScheduler, IExecutePendingTasks
{
	private readonly LinkedList<Task> queue = new LinkedList<Task>();
	private readonly SynchronizationContext unitySynchronizationContext;

	public UnityTaskScheduler(SynchronizationContext context)
	{
		unitySynchronizationContext = context;
	}

	public void ExecutePendingTasks()
	{
		while (true)
		{
			Task task;
			lock (queue)
			{
				if (queue.Count == 0)
				{
					break;
				}

				task = queue.First.Value;
				queue.RemoveFirst();
			}

			if (task != null)
			{
				var result = TryExecuteTask(task);
				if (result == false)
				{
					throw new InvalidOperationException();
				}
			}
		}
	}

	protected override IEnumerable<Task> GetScheduledTasks()
	{
		lock (queue)
		{
			return queue.ToArray();
		}
	}

	protected override void QueueTask(Task task)
	{
		lock (queue)
		{
			queue.AddLast(task);
		}
	}

	protected override bool TryExecuteTaskInline(Task task, bool taskWasPreviouslyQueued)
	{
		if (SynchronizationContext.Current != unitySynchronizationContext)
		{
			return false;
		}

		if (taskWasPreviouslyQueued)
		{
			lock (queue)
			{
				queue.Remove(task);
			}
		}

		return TryExecuteTask(task);
	}
}
