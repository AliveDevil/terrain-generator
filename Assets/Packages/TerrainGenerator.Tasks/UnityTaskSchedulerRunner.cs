using UnityEngine;
using UnityEngine.Profiling;

namespace TerrainGenerator.Tasks
{
	public class UnityTaskSchedulerRunner : MonoBehaviour
	{
		public IExecutePendingTasks TaskScheduler { get; set; }

		private void Update()
		{
			if (TaskScheduler != null)
			{
				Profiler.BeginSample(name);
				TaskScheduler.ExecutePendingTasks();
				Profiler.EndSample();
			}
		}
	}
}
