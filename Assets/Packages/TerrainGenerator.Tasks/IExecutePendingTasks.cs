namespace TerrainGenerator.Tasks
{
	public interface IExecutePendingTasks
	{
		void ExecutePendingTasks();
	}
}
