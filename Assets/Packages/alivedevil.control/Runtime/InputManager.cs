using Cinemachine;
using System;
using UnityEngine;

public class InputManager : MonoBehaviour
{
	private int activeController = -1;
	[SerializeField]
	private ViewCameraCouple[] couples = null;
	private InputState inputState;

	private ViewCameraCouple Active => couples[activeController];

	public void Begin()
	{
		couples = new ViewCameraCouple[inputState.InputControllers.Count];
		var i = 0;
		foreach (var item in inputState.InputControllers)
		{
			couples[i].controller = item;
			couples[i].camera = item.ControlledCamera;
			i += 1;
		}

		activeController = 0;
		Active.controller.SwitchTo();
		Active.camera.MoveToTopOfPrioritySubqueue();
	}

	private void FixedUpdate()
	{
		if (activeController == -1)
		{
			return;
		}
		Active.controller.Movement();
	}

	private void LateUpdate()
	{
		if (activeController == -1)
		{
			return;
		}
		Active.controller.CameraUpdate();
	}

	private void OnEnable()
	{
		inputState = InputState.PerScene(gameObject);
		inputState.InputManager = this;
	}

	private void Update()
	{
		if (activeController == -1)
		{
			return;
		}

		if (Input.GetKeyDown(KeyCode.F))
		{
		}
		else
		{
			Active.controller.CheckInput();
		}
	}

	[Serializable]
	private struct ViewCameraCouple
	{
		public CinemachineVirtualCamera camera;
		public InputController controller;
	}
}
