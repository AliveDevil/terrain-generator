using UnityEngine;

public struct Rotation
{
	private Quaternion quaternion;
	private bool valid;
	private float yaw, pitch, roll;

	public float Pitch
	{
		get
		{
			return pitch;
		}
		set
		{
			SetPitch(value);
		}
	}

	public Quaternion Quaternion
	{
		get
		{
			if (!valid)
			{
				valid = true;
				float halfRoll = Mathf.Deg2Rad * roll * 0.5f;
				float halfPitch = Mathf.Deg2Rad * pitch * 0.5f;
				float halfYaw = Mathf.Deg2Rad * yaw * 0.5f;

				float sinRoll = Mathf.Sin(halfRoll);
				float cosRoll = Mathf.Cos(halfRoll);
				float sinPitch = Mathf.Sin(halfPitch);
				float cosPitch = Mathf.Cos(halfPitch);
				float sinYaw = Mathf.Sin(halfYaw);
				float cosYaw = Mathf.Cos(halfYaw);

				quaternion = new Quaternion(
					(cosYaw * sinPitch * cosRoll) + (sinYaw * cosPitch * sinRoll),
					(sinYaw * cosPitch * cosRoll) - (cosYaw * sinPitch * sinRoll),
					(cosYaw * cosPitch * sinRoll) - (sinYaw * sinPitch * cosRoll),
					(cosYaw * cosPitch * cosRoll) + (sinYaw * sinPitch * sinRoll));
			}
			return quaternion;
		}
	}

	public float Roll
	{
		get
		{
			return roll;
		}
		set
		{
			SetRoll(value);
		}
	}

	public float Yaw
	{
		get
		{
			return yaw;
		}
		set
		{
			SetYaw(value);
		}
	}

	public void SetPitch(float value)
	{
		value %= 360;
		if (pitch == value)
			return;
		pitch = value;
		valid = false;
	}

	public void SetRoll(float value)
	{
		value %= 360;
		if (roll == value)
			return;
		roll = value;
		valid = false;
	}

	public void SetYaw(float value)
	{
		value %= 360;
		if (yaw == value)
			return;
		yaw = value;
		valid = false;
	}
}
