using System.Collections.Generic;

public class InputState : UnitySingleton<InputState>
{
	private List<InputController> inputControllers = new List<InputController>();
	private IReadOnlyCollection<InputController> inputControllersReadOnly;

	public IIndexable<int, InputController> InputController => new InputControllerAccessor(this);

	public IReadOnlyCollection<InputController> InputControllers => inputControllersReadOnly ?? (inputControllersReadOnly = inputControllers.AsReadOnly());

	public InputManager InputManager { get; set; }

	public void Register(InputController inputController)
	{
		inputControllers.Add(inputController);
	}

	protected override void OnInitialize()
	{
	}

	private struct InputControllerAccessor : IIndexable<int, InputController>
	{
		private readonly InputState state;

		public InputController this[int key] => state.inputControllers[key];

		public InputControllerAccessor(InputState state)
		{
			this.state = state;
		}
	}
}
