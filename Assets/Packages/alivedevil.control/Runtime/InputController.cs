using Cinemachine;
using UnityEngine;

public abstract class InputController : MonoBehaviour
{
	public abstract CinemachineVirtualCamera ControlledCamera { get; }

	public void CameraUpdate()
	{
		OnCameraUpdate();
	}

	public void CheckInput()
	{
		OnCheckInput();
	}

	public void Movement()
	{
		OnMovement();
	}

	public void SwitchFrom()
	{
		OnSwitchFrom();
	}

	public void SwitchTo()
	{
		OnSwitchTo();
	}

	protected virtual void OnEnable()
	{
		InputState.PerScene(gameObject).Register(this);
	}

	protected abstract void OnCameraUpdate();

	protected abstract void OnCheckInput();

	protected abstract void OnMovement();

	protected abstract void OnSwitchFrom();

	protected abstract void OnSwitchTo();
}
