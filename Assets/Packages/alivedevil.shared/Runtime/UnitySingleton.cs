﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public abstract class UnitySingleton<T> : MonoBehaviour where T : UnitySingleton<T>
{
	private readonly static Dictionary<int, T> perSceneInstances = new Dictionary<int, T>();
	private static T global;

	public static T Active
	{
		get
		{
			return PerScene(Singleton.ActiveScene);
		}
	}

	public static T Global
	{
		get
		{
			if (!global)
			{
				var g = new GameObject("Global");
				DontDestroyOnLoad(g);
				global = g.AddComponent<T>();
			}
			return global;
		}
	}

	public static T PerScene(GameObject gameObject)
	{
		var scene = gameObject.scene;
		if (scene == Global.gameObject.scene)
		{
			return global;
		}
		var sceneIndex = scene.GetHashCode();
		T instance;
		if (perSceneInstances.TryGetValue(sceneIndex, out instance))
		{
			return instance;
		}

		var root = scene.GetRootGameObjects();
		foreach (var item in root)
		{
			var t = item.GetComponent<T>();
			if (t)
			{
				perSceneInstances[sceneIndex] = t;
				return t;
			}
		}

		var g = new GameObject();
		SceneManager.MoveGameObjectToScene(g, scene);
		instance = g.AddComponent<T>();
		perSceneInstances[sceneIndex] = instance;
		return instance;
	}

	public void Initialize()
	{
		OnInitialize();
	}

	protected abstract void OnInitialize();

	private static void Application_quitting()
	{
		Application.quitting -= Application_quitting;
		SceneManager.sceneUnloaded -= SceneManager_sceneUnloaded;
	}

	private static T CreateInstance(Scene scene)
	{
		var g = new GameObject();
		SceneManager.MoveGameObjectToScene(g, scene);
		var t = g.AddComponent<T>();
		t.Initialize();
		return t;
	}

	private static T PerScene(Scene scene)
	{
		T instance;
		var sceneIndex = scene.GetHashCode();
		if (!perSceneInstances.TryGetValue(sceneIndex, out instance))
		{
			var g = new GameObject();
			SceneManager.MoveGameObjectToScene(g, scene);
			instance = g.AddComponent<T>();
			perSceneInstances[sceneIndex] = instance;
		}
		return instance;
	}

	private static void SceneManager_sceneUnloaded(Scene scene)
	{
		perSceneInstances.Remove(scene.GetHashCode());
	}

	[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
	private static void Setup()
	{
		Application.quitting += Application_quitting;
		SceneManager.sceneUnloaded += SceneManager_sceneUnloaded;
	}
}
