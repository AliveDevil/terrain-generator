using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

[DebuggerStepThrough]
public class JsArray<T> : IEnumerable<T> where T : struct
{
	private T[] array;

	public SafeIndexer Safe => new SafeIndexer(this);

	public T this[int index]
	{
		get { return array[index]; }
		set { Set(index, value); }
	}

	public int Length => array.Length;

	public JsArray()
	{
		array = new T[0];
	}

	public JsArray(int length)
	{
		array = new T[length];
	}

	public JsArray(T[] source)
	{
		array = source;
	}

	public void Add(T value)
	{
		Splice(array.Length, value);
	}

	public bool Contains(T value)
	{
		for (int i = 0; i < Length; i++)
		{
			if (Equals(array[i], value))
			{
				return true;
			}
		}
		return false;
	}

	public void Push(T value)
	{
		Splice(array.Length, value);
	}

	public void Reverse(int start, int end)
	{
		Array.Reverse(array, start, end - start);
	}

	public T Set(int index, T value)
	{
		if (index >= array.Length)
		{
			Array.Resize(ref array, index + 1);
		}
		array[index] = value;

		return value;
	}

	public void Splice(int index, T value)
	{
		var count = array.Length - index;
		Array.Resize(ref array, array.Length + 1);
		Array.Copy(array, index, array, index + 1, count);
		array[index] = value;
	}

	public void Unshift(T value)
	{
		Splice(0, value);
	}

	public T[] Value() => array;

	IEnumerator<T> IEnumerable<T>.GetEnumerator() => ((IEnumerable<T>)array).GetEnumerator();

	IEnumerator IEnumerable.GetEnumerator() => array.GetEnumerator();

	public struct SafeIndexer
	{
		private readonly JsArray<T> parent;

		public T? this[int index]
		{
			get
			{
				if (index >= 0 && index < parent.array.Length)
				{
					return parent.array[index];
				}
				return null;
			}
			set
			{
				if (value.HasValue)
				{
					parent.Set(index, value.Value);
				}
			}
		}

		public SafeIndexer(JsArray<T> parent)
		{
			this.parent = parent;
		}
	}
}
