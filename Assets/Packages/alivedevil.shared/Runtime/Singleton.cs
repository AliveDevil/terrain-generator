using UnityEngine;
using UnityEngine.SceneManagement;

public static class Singleton
{
	public static Scene ActiveScene { get; private set; }

	private static void Application_quitting()
	{
		Application.quitting -= Application_quitting;
		SceneManager.activeSceneChanged -= SceneManager_activeSceneChanged;
	}

	private static void SceneManager_activeSceneChanged(Scene current, Scene next)
	{
		ActiveScene = next;
	}

	[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
	private static void Setup()
	{
		Application.quitting += Application_quitting;
		SceneManager.activeSceneChanged += SceneManager_activeSceneChanged;
	}
}

public abstract class Singleton<T> : MonoBehaviour where T : Singleton<T>
{
}
