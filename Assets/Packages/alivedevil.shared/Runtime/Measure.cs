using System;
using System.Diagnostics;

public delegate void MeasureEventHandler(string name, TimeSpan time);

public struct Measure : IDisposable
{
	private MeasureEventHandler handler;
	private string name;
	private Stopwatch watch;

	public Measure(string name, MeasureEventHandler handler)
	{
		this.name = name;
		this.handler = handler;
		watch = Stopwatch.StartNew();
	}

	public void Dispose()
	{
		watch.Stop();
		handler?.Invoke(name, watch.Elapsed);
	}
}
