public interface IIndexable<TKey, TItem>
{
	TItem this[TKey key] { get; }
}
