using d3.delaunay;
using System;
using TerrainGenerator.Contracts;
using TerrainGenerator.Data.Assets;
using UnityEngine;

namespace TerrainGenerator.Data
{
	public class BiomeGraph : StateBehaviour, IBiomeGraph
	{
		[SerializeField]
		private BiomeSetAsset asset = null;
		private Delaunay delaunay;
		private IMeasure measure;
		private Voronoi voronoi;

		public BiomeSetAsset BiomeSet
		{
			get { return asset; }
			set { asset = value; }
		}

		public IMeasure Measure => measure ?? (measure = TerrainState.Measure);

		public BiomeAsset Ocean => asset.Ocean;

		[Obsolete]
		public void Build()
		{
			Initialize();
		}

		public BiomeAsset Find(float temperature, float rainfall)
		{
			//using (Measure.Measure("Biome Find"))
			{
				var region = voronoi.Find(temperature, rainfall);

				return asset.Biomes[region];
			}
		}

		protected override void OnInitialize()
		{
			Vector2[] biomeConfig;
			using (Measure.Measure("Biomes Build"))
			{
				var biomes = asset.Biomes;
				biomeConfig = new Vector2[biomes.Length];

				for (int i = 0, n = biomeConfig.Length; i < n; i++)
				{
					var temperature = biomes[i].MeanTemperature;
					var rainfall = biomes[i].MeanRainfall;

					biomeConfig[i] = new Vector2(temperature, rainfall);
				}
			}
			using (Measure.Measure("Biomes Graph"))
			{
				delaunay = new Delaunay(biomeConfig);
				voronoi = delaunay.Voronoi(new Rect(-1, 0, 2, 1));
			}
		}

		protected override void OnRegister()
		{
			TerrainState.BiomeGraph = this;
		}

		protected override void OnSetup()
		{
		}

		protected override void OnTearDown()
		{
		}
	}
}
