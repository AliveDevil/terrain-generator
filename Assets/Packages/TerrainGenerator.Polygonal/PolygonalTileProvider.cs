using d3.delaunay;
using mapbox;
using redblobgames;
using System;
using System.Collections.Generic;
using System.Text;
using TerrainGenerator.Contracts;
using TerrainGenerator.Extensions;
using UnityEngine;
using Random = System.Random;

namespace TerrainGenerator.Terrain
{
	public class PolygonalTileProvider : TileProvider
	{
		private delegate void InterpolateAction<T>(int[] surrounds, out T data);

		private IBiomeGraph biomeGraph;
		private Data data;
		private Random elevationRandom;
		private float maxRainfall;
		private float maxTemperature;
		private float minTemperature;
		private Random rainfallRandom;
		private Random sourceRandom;
		private Combined surfaceMesh;
		private Random surfaceRandom;
		private Random temperatureRandom;

		public RectInt WorldSize
		{
			get
			{
				var size = TerrainController.Size;
				var mod = TerrainController.ChunkSize;
				size.min *= mod;
				size.max *= mod;
				return size;
			}
		}

		public IBiomeGraph BiomeGraph => biomeGraph ?? (biomeGraph = TerrainState.BiomeGraph);

		public float GetRainfall(Vector2 position, bool interpolate)
		{
			position.x = Mathf.Round(position.x);
			position.y = Mathf.Round(position.y);

			var region = surfaceMesh.Voronoi.Find(position);
			var regionPoint = data.points[region];
			var regionFall = data.r_rainfall[region];

			if (!interpolate)
			{
				return regionFall;
			}

			if (regionPoint == position)
			{
				return regionFall;
			}

			var nearestV1 = -1;
			var nearestV2 = -1;
			var nearestBarycentric = Interpolate(surfaceMesh.Voronoi, region, regionPoint, position, out nearestV1, out nearestV2);

			if (nearestBarycentric.HasValue)
			{
				var nearest = nearestBarycentric.Value;
				return
					regionFall * nearest.z
					+ data.t_rainfall[nearestV1] * nearest.x
					+ data.t_rainfall[nearestV2] * nearest.y;
			}

			var surrounds = surfaceMesh.Voronoi.CellRaw(region);
			var surroundPoints = new Vector2[surrounds.Length];
			for (int i = surrounds.Length - 1; i >= 0; i--)
			{
				surroundPoints[i] = data.points[surrounds[i]];
			}
			var builder = new StringBuilder();
			builder.AppendLine("Cannot interpolate rainfall");
			builder.AppendLine($"Position: {position}");
			builder.AppendLine($"Region: {region} / {regionPoint}");
			builder.AppendLine($"Surrounds: {string.Join(", ", surroundPoints)}");

			throw new Exception(builder.ToString());
		}

		public float GetTemperature(Vector2 position, bool interpolate)
		{
			position.x = Mathf.Round(position.x);
			position.y = Mathf.Round(position.y);

			var region = surfaceMesh.Voronoi.Find(position);
			var regionPoint = data.points[region];
			var regionTemperature = data.r_temperature[region];

			if (!interpolate)
			{
				return regionTemperature;
			}

			if (regionPoint == position)
			{
				return regionTemperature;
			}

			var nearestV1 = -1;
			var nearestV2 = -1;
			var nearestBarycentric = Interpolate(surfaceMesh.Voronoi, region, regionPoint, position, out nearestV1, out nearestV2);

			if (nearestBarycentric.HasValue)
			{
				var nearest = nearestBarycentric.Value;
				return
					regionTemperature * nearest.z
					+ data.t_temperature[nearestV1] * nearest.x
					+ data.t_temperature[nearestV2] * nearest.y;
			}
			throw new Exception("Cannot interpolate temperature.");
		}

		protected override void OnElevate(TerrainSample sample)
		{
			if (sample.Surface)
			{
				var region = surfaceMesh.Voronoi.Find(sample.Position);
				var regionPoint = data.points[region];
				var regionHeight = data.r_elevation[region];

				if (sample.Position == regionPoint)
				{
					sample.Elevation = regionHeight;
					return;
				}

				/* if (p1 == sample.Position)
					{
						sample.Elevation = data.t_elevation[v1];
						return;
					}
				*/

				var nearestV1 = -1;
				var nearestV2 = -1;
				var nearestBarycentric = Interpolate(surfaceMesh.Voronoi, region, regionPoint, sample.Position, out nearestV1, out nearestV2);

				if (nearestBarycentric.HasValue)
				{
					var nearest = nearestBarycentric.Value;
					sample.Elevation = Mathf.RoundToInt(
						regionHeight * nearest.z
						+ data.t_elevation[nearestV1] * nearest.x
						+ data.t_elevation[nearestV2] * nearest.y);
					return;
				}

				sample.Elevation = 0;
				Debug.LogWarning($"Cannot elevate. Point {sample.Position} is not on Region, Triangle Corner, Inside Triangle or on Edges.");
			}
			else
			{
				sample.Elevation = 0;
			}
		}

		protected override void OnInitializeTileProvider()
		{
			CreateSurface();
			CreateTemperature();
			CreateRainfall();
			CreateElevation();
		}

		protected override void OnRegisterTileProvider()
		{
			Depends<IBiomeGraph>();
		}

		protected override void OnSample(TerrainSample sample)
		{
			var region = surfaceMesh.Voronoi.Find(sample.Position);
			sample.Surface = data.r_surface[region];
			if (sample.Surface)
			{
				sample.Temperature = GetBoundedValue(sample.Position, region, data.r_temperature, data.r_surface, () => -1);
				sample.Rainfall = GetBoundedValue(sample.Position, region, data.r_rainfall, data.r_surface, () => 0);
			}
		}

		protected override void OnSetupTileProvider()
		{
			sourceRandom = new Random(0);
			surfaceRandom = new Random(sourceRandom.Next());
			temperatureRandom = new Random(sourceRandom.Next());
			rainfallRandom = new Random(sourceRandom.Next());
			elevationRandom = new Random(sourceRandom.Next());
		}

		private static Vector3 Barycentric(Vector2 p, Vector2 a, Vector2 b, Vector2 c)
		{
			Vector2 v0 = b - a, v1 = c - a, v2 = p - a;
			float d00 = Vector2.Dot(v0, v0);
			float d01 = Vector2.Dot(v0, v1);
			float d11 = Vector2.Dot(v1, v1);
			float d20 = Vector2.Dot(v2, v0);
			float d21 = Vector2.Dot(v2, v1);
			float denom = 1 / (d00 * d11 - d01 * d01);
			//			v = (d11 * d20 - d01 * d21) / denom;
			//			w = (d00 * d21 - d01 * d20) / denom;
			//u = 1.0f - v - w;

			var v = (d11 * d20 - d01 * d21) * denom;
			var w = (d00 * d21 - d01 * d20) * denom;
			var u = 1 - v - w;

			return new Vector3(u, v, w);
		}

		private static float GetBoundedValue(Vector2 v, int index, float[] data, bool[] surface, Func<float> defaultFunc)
		{
			if ((index - surface.Length) >= 0)
			{
				return defaultFunc();
			}
			return data[index];
		}

		private static float map(float x, float a, float b, float c, float d)
		{
			return (x - a) / (b - a) * (d - c) + c;
		}

		private static float RangedFloat(Random random, float minimum, float maximum)
		{
			return Mathf.Round((float)random.NextDouble() * 20 * (maximum - minimum)) / 20 + minimum;
		}

		private static int RangedInt(Random random, int minimum, int maximum)
		{
			return Mathf.RoundToInt((float)random.NextDouble() * 8 * (maximum - minimum)) / 8 + minimum;
		}

		private void CreateElevation()
		{
			data.r_elevation = new int[data.r_surface.Length];
			for (int i = 0, n = data.r_surface.Length; i < n; i++)
			{
				if (data.r_surface[i])
				{
					var biome = BiomeGraph.Find(data.r_temperature[i], data.r_rainfall[i]);
					data.r_elevation[i] = RangedInt(elevationRandom, 0, biome.TerrainAmplitude);
					for (int o = 0; o < 3; o++)
					{
						data.r_elevation[i] = Mathf.Max(0, data.r_elevation[i] + RangedInt(elevationRandom, -biome.TerrainAmplitude / (o + 2), biome.TerrainAmplitude / (o + 2)));
					}
				}
			}

			// TODO Triangle Elevation
			// perform Water-Constraints
			data.t_elevation = new int[surfaceMesh.Voronoi.Circumcenters.Length];
			int[] known = new int[data.points.Length / 32 + 1];
			Stack<int> boundaryPoints = new Stack<int>();
			for (int i = 0, n = surfaceMesh.Delaunay.Hull.Length; i < n; i++)
			{
				var h = surfaceMesh.Delaunay.Hull[i];
				var t = surfaceMesh.Delaunay.Triangles[h];
				boundaryPoints.Push(t);
				known[t / 32] |= 1 << (t % 32);
				if ((t - data.r_surface.Length) < 0)
				{
					data.r_surface[t] = false;
					data.r_elevation[t] = 0;
				}
			}

			while (boundaryPoints.Count > 0)
			{
				var r = boundaryPoints.Pop();

				foreach (var item in surfaceMesh.Dual.r_circulate_r(r))
				{
					if ((known[item / 32] & (1 << (item % 32))) == 0)
					{
						known[item / 32] |= 1 << (item % 32);
						if (surfaceMesh.Dual.r_anyghost(r))
						{
							boundaryPoints.Push(item);
						}
					}
					if ((item - data.r_surface.Length) < 0)
					{
						data.r_surface[item] = false;
						data.r_elevation[item] = 0;
					}
				}
			}

			InterpolateTriangles(ref surfaceMesh, out data.t_elevation, (int[] surrounds, out int elevation) =>
			{
				var anyOcean = false;
				for (int i = 0, m = surrounds.Length; i < m; i++)
				{
					var region = surrounds[i];
					var boundary = (region - data.r_surface.Length) >= 0;
					anyOcean |= boundary || !data.r_surface[region];
				}
				if (anyOcean)
				{
					elevation = 0;
				}
				else
				{
					elevation = Mathf.RoundToInt((GetElevation(surrounds[0]) + GetElevation(surrounds[1]) + GetElevation(surrounds[2])) / 3);
				}
			});
		}

		private void CreateRainfall()
		{
			data.r_rainfall = new float[data.r_surface.Length];
			for (int i = 0, n = data.r_rainfall.Length; i < n; i++)
			{
				data.r_rainfall[i] = RangedFloat(rainfallRandom, 0, 1);
			}

			InterpolateTriangles(ref surfaceMesh, out data.t_rainfall, (int[] surrounds, out float rainfall) =>
			{
				var total = 0f;
				for (int i = 0, m = surrounds.Length; i < m; i++)
				{
					total += GetBoundedValue(data.points[surrounds[i]], surrounds[i], data.r_rainfall, data.r_surface, () => 0);
				}
				rainfall = total / surrounds.Length;
			});
		}

		private void CreateSurface()
		{
			var size = WorldSize;
			var width = size.width;
			var height = size.height;
			var xMin = size.xMin;
			var yMin = size.yMin;
			var xMax = size.xMax;
			var yMax = size.yMax;

			int c = (width * height) / (24 * 24);
			var points = data.points = new Vector2[c + 4];
			data.r_surface = new bool[c];
			for (int i = 0; i < c; i++)
			{
				int x = 0;
				int y = 0;
				bool found = false;
				while (!found)
				{
					x = RangedInt(surfaceRandom, xMin, xMax);
					y = RangedInt(surfaceRandom, yMin, yMax);

					found = true;
					for (int j = i - 1; j >= 0; j--)
					{
						if (points[j].x == x && points[j].y == y)
						{
							found = false;
						}
					}
				}
				points[i] = new Vector2(x, y);
				data.r_surface[i] = RangedInt(surfaceRandom, 0, 10) < 6;
			}
			points[c + 0] = new Vector2(xMin - xMax, yMin - yMax);
			points[c + 1] = new Vector2(xMin - xMax, yMax + yMax);
			points[c + 2] = new Vector2(xMax + xMax, yMin - yMax);
			points[c + 3] = new Vector2(xMax + xMax, yMax + yMax);

			surfaceMesh.Delaunay = new Delaunay(points);
			surfaceMesh.Voronoi = surfaceMesh.Delaunay.Voronoi(Rect.MinMaxRect(xMin, yMin, xMax, yMax));
			surfaceMesh.Dual = surfaceMesh.Delaunay.DualMesh();
		}

		private void CreateTemperature()
		{
			var size = WorldSize;
			var width = size.width;
			var height = size.height;
			var xMin = size.xMin;
			var yMin = size.yMin;
			var xMax = size.xMax;
			var yMax = size.yMax;

			data.r_temperature = new float[data.r_surface.Length];
			for (int i = 0, n = data.r_temperature.Length; i < n; i++)
			{
				var v = data.points[i];
				var global = 1 - Mathf.Abs(v.y) / (yMax / 2f);
				var local = RangedFloat(temperatureRandom, -1, 1);
				data.r_temperature[i] = map(global * 4 + local, -5, 5, -1, 1);
			}

			InterpolateTriangles(ref surfaceMesh, out data.t_temperature, (int[] surrounds, out float temperature) =>
			{
				var total = 0f;
				for (int i = 0, m = surrounds.Length; i < m; i++)
				{
					total += GetBoundedValue(data.points[surrounds[i]], surrounds[i], data.r_temperature, data.r_surface, () => -1);
				}
				temperature = total / surrounds.Length;
			});
		}

		private int GetElevation(int index)
		{
			if ((index - data.r_surface.Length) >= 0)
				return 0;
			return data.r_elevation[index];
		}

		private Vector3? Interpolate(Voronoi voronoi, int region, Vector2 regionPoint, Vector2 samplePoint, out int nearestV1, out int nearestV2)
		{
			var v = voronoi.CellRaw(region);
			nearestV1 = -1;
			nearestV2 = -1;
			var nearestBarycentric = default(Vector3?);
			var nearestDistance = float.PositiveInfinity;
			for (int i = 0, n = v.Length, v0 = 0, v1 = v[n - 1]; i < n; i++)
			{
				v0 = v1;
				v1 = v[i];

				var p0 = voronoi.Circumcenters[v0];
				var p1 = voronoi.Circumcenters[v1];

				if (TriangleExtensions.accuratePointInTriangle(p0, regionPoint, p1, samplePoint))
				{
					var c = Delaunator.circumcenter(p0, p1, regionPoint);

					var d = (samplePoint - c).sqrMagnitude;
					if (d < nearestDistance)
					{
						nearestDistance = d;
						nearestV1 = v0;
						nearestV2 = v1;
						nearestBarycentric = Barycentric(samplePoint, p0, p1, regionPoint);
					}
				}
			}
			return nearestBarycentric;
		}

		private void InterpolateTriangles<T>(ref Combined mesh, out T[] data, InterpolateAction<T> callback)
		{
			data = new T[mesh.Voronoi.Circumcenters.Length];
			for (int i = 0, n = data.Length; i < n; i++)
			{
				var surrounds = mesh.Dual.t_circulate_r(i);
				callback(surrounds, out data[i]);
			}
		}

		private struct Combined
		{
			public Delaunay Delaunay;
			public DualMesh Dual;
			public Voronoi Voronoi;
		}

		private struct Data
		{
			public Vector2[] points;
			public int[] r_elevation;
			public float[] r_rainfall;
			public bool[] r_surface;
			public float[] r_temperature;
			public int[] t_elevation;
			public float[] t_rainfall;
			public float[] t_temperature;
		}
	}
}
