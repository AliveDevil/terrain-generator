using System.Collections.Generic;
using UnityEngine;

public class ViewStack : MonoBehaviour
{
	[SerializeField]
	private Transform content = null;
	[SerializeField]
	private View initialView = null;
	[SerializeField]
	private View parentView = null;

	private Stack<View> views = new Stack<View>();

	public ViewStack ParentStack
	{
		get
		{
			return parentView ? parentView.ViewStack : null;
		}
	}

	public ViewStack Root
	{
		get
		{
			var stack = this;
			while (stack.ParentStack)
				stack = stack.ParentStack;
			return stack;
		}
	}

	public View View
	{
		get
		{
			if (views.Count > 0)
			{
				return views.Peek();
			}
			return null;
		}
	}

	public void Close()
	{
		if (views.Count > 0)
		{
			var view = views.Pop();
			Destroy(view.gameObject);
		}
		if (views.Count > 0)
		{
			View.gameObject.SetActive(true);
		}
	}

	public T Push<T>(T prefab) where T : View
	{
		var disabledGameObject = new GameObject();
		disabledGameObject.SetActive(false);
		var t = Instantiate(prefab, disabledGameObject.transform, false);
		t.gameObject.SetActive(false);
		t.transform.SetParent(content, false);
		Destroy(disabledGameObject);

		t.ViewStack = this;
		if (views.Count > 0)
		{
			View.gameObject.SetActive(false);
		}
		views.Push(t);
		t.gameObject.SetActive(true);
		return t;
	}

	public T Replace<T>(T prefab) where T : View
	{
		if (views.Count > 0)
		{
			var oldView = views.Pop();
			Destroy(oldView.gameObject);
		}

		var disabledGameObject = new GameObject();
		disabledGameObject.SetActive(false);
		var t = Instantiate(prefab, disabledGameObject.transform, false);
		t.gameObject.SetActive(false);
		t.transform.SetParent(content, false);
		Destroy(disabledGameObject);
		t.ViewStack = this;
		views.Push(t);
		t.gameObject.SetActive(true);
		return t;
	}

	private void OnEnable()
	{
		Push(initialView);
	}
}
