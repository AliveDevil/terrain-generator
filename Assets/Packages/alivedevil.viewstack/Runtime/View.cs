using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class View : MonoBehaviour
{
	public ViewStack ViewStack { get; set; }
}
