using System;
using TerrainGenerator.ThirdParty.OpenSimplexNoise;
using UnityEngine;

namespace TerrainGenerator.Extensions
{
	public static class OpenSimplexNoiseExtensions
	{
		private const double F2 = 1.4142135623730950488016887242097;
		private const double F3 = 1.1547005383792515290182975610039;

		public static float EvaluateOctave(this OpenSimplexNoise noise, Vector2 v, float scale, float persistence, int octaves) => EvaluateOctave(noise, v.x, v.y, scale, persistence, octaves);

		public static float EvaluateOctave(this OpenSimplexNoise noise, float x, float y, float scale, float persistence, int octaves)
		{
			if (octaves < 0 || octaves == 0)
			{
				throw new ArgumentOutOfRangeException("octaves", "Must not be lesser or equal to 0");
			}

			float resultUnscaled = 0;
			float totalAmplitude = 0;
			float amplitude = 1;

			for (int i = 0; i < octaves; i++)
			{
				resultUnscaled += (float)(noise.EvaluateScaled(x * scale, y * scale) * amplitude);
				scale /= 2;
				totalAmplitude += amplitude;
				amplitude *= persistence;
			}

			return resultUnscaled / totalAmplitude;
		}

		public static float EvaluateOctave(this OpenSimplexNoise noise, Vector3 v, float scale, float persistence, int octaves) => EvaluateOctave(noise, v.x, v.y, v.z, scale, persistence, octaves);

		public static float EvaluateOctave(this OpenSimplexNoise noise, float x, float y, float z, float scale, float persistence, int octaves)
		{
			if (octaves < 0 || octaves == 0)
			{
				throw new ArgumentOutOfRangeException("octaves", "Must not be lesser or equal to 0");
			}

			float resultUnscaled = 0;
			float totalAmplitude = 0;
			float amplitude = 1;

			for (int i = 0; i < octaves; i++)
			{
				resultUnscaled += (float)(noise.EvaluateScaled(x * scale, y * scale, z * scale) * amplitude);
				scale /= 2;
				totalAmplitude += amplitude;
				amplitude *= persistence;
			}

			return resultUnscaled / totalAmplitude;
		}

		public static float EvaluateOctaveSimple(this OpenSimplexNoise noise, Vector2 v, float scale, int octaves) => EvaluateOctave(noise, v, scale, 1, octaves);

		public static float EvaluateOctaveSimple(this OpenSimplexNoise noise, float x, float y, float scale, int octaves) => EvaluateOctave(noise, x, y, scale, 1, octaves);

		public static float EvaluateOctaveSimple(this OpenSimplexNoise noise, Vector3 v, float scale, int octaves) => EvaluateOctave(noise, v, scale, 1, octaves);

		public static float EvaluateOctaveSimple(this OpenSimplexNoise noise, float x, float y, float z, float scale, int octaves) => EvaluateOctave(noise, x, y, z, scale, 1, octaves);

		public static double EvaluateScaled(this OpenSimplexNoise noise, float x, float y) => noise.Evaluate(x, y) * F2;

		public static double EvaluateScaled(this OpenSimplexNoise noise, float x, float y, float z) => noise.Evaluate(x, y, z) * F3;
	}
}
