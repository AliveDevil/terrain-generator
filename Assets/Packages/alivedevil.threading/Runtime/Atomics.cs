using System.Threading;

namespace TerrainGenerator.Core
{
	public static class Atomics
	{
		public static bool SetFailed(ref int slot, int value, int comparand)
		{
			return comparand != Interlocked.CompareExchange(ref slot, value, comparand);
		}

		public static bool SetFailed(ref int slot, int value)
		{
			return SetFailed(ref slot, value, slot);
		}
	}
}
