using System.Diagnostics;
using System.Threading;
using Debug = UnityEngine.Debug;

namespace TerrainGenerator.Core
{
	public struct RWLock
	{
		private const int WRITE_FLAG = 0x01 << 31;
		private int flags;

		[Conditional("DEBUG")]
		public void Check(int readers, bool writeLock)
		{
			Debug.Assert(readers == (Thread.VolatileRead(ref flags) & ~WRITE_FLAG));
			Debug.Assert(writeLock && 0 > Thread.VolatileRead(ref flags) || !writeLock && 0 <= Thread.VolatileRead(ref flags));
		}

		public void EnterReadLock()
		{
			while (!TryEnterReadLock()) Thread.Sleep(0);
		}

		public void ExitReadLock()
		{
			Interlocked.Decrement(ref flags);
		}

		public void ExitWriteLock(object sync)
		{
			int tmp;
			do tmp = flags;
			while (Atomics.SetFailed(ref flags, tmp & ~WRITE_FLAG, tmp));
			Monitor.Exit(sync);
		}

		public bool TryEnterReadLock()
		{
			// less expensive check for writer, before attempting interlocked operations
			// this ends up being more expensive when there are no writers
			if (0 > Thread.VolatileRead(ref flags)) return false;
			// writer may have acquired write lock and flag bit between last check and now,
			// so verify no change, and if it has changed, back out read lock acquisition
			int tmp;
			if (0 > (tmp = Interlocked.Increment(ref flags)))
			{
				Interlocked.Decrement(ref flags);
				return false;
			}
			Debug.Assert(tmp > 0);
			return true;
		}

		public bool TryEnterWriteLock(object sync)
		{
			if (!Monitor.TryEnter(sync)) return false;
			// if write lock already acquired, then recursive write lock
			if (flags < 0)
			{
				Monitor.Exit(sync);
				throw new System.Threading.LockRecursionException();
			}
			// this test-and-set tries to write WRITE_MASK directly and expects 0 readers
			// if some readers are still active, it returns acquisition failure
			if (Atomics.SetFailed(ref flags, WRITE_FLAG, 0))
			{
				Monitor.Exit(sync);
				return false;
			}
			Debug.Assert(0 > Thread.VolatileRead(ref flags));
			return true;
		}
	}
}
