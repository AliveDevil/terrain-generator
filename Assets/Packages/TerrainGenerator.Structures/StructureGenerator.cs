using FieldTreeStructure.Spatial;
using System;
using System.Collections.Generic;
using System.Linq;
using TerrainGenerator.Contracts;
using TerrainGenerator.Data.Assets;
using UnityEngine;
using Random = System.Random;

namespace TerrainGenerator.Structures
{
	public class StructureGenerator : StateBehaviour, IStructureGenerator
	{
		private IBiomeGraph biomeGraph;
		private ITerrainController terrainController;

		public IBiomeGraph BiomeGraph => biomeGraph ?? (biomeGraph = TerrainState.BiomeGraph);

		public ITerrainController TerrainController => terrainController ?? (terrainController = TerrainState.TerrainController);

		public void Generate(IStructureContainer structureContainer, Vector2Int chunkPosition, int chunkSize, Func<Vector2Int, ITile> tileAccessor, int initializer)
		{
			var context = new StructureGeneratorContext(this, new Random(initializer), chunkPosition, chunkSize, structureContainer, tileAccessor);
			context.CreatePlacement();
			context.Annotate();
			context.Filter();
			context.PostProcess();
			context.Spawn();
		}

		protected override void OnInitialize()
		{
		}

		protected override void OnRegister()
		{
			TerrainState.StructureGenerator = this;
			Depends<IBiomeGraph>();
			Depends<ITerrainController>();
		}

		protected override void OnSetup()
		{
		}

		protected override void OnTearDown()
		{
		}

		private class Leaf
		{
			public DensitySpatial Density { get; }

			public int DensityRadius { get; private set; }

			public int Overlapping { get; set; }

			public PlacementSpatial Placement { get; }

			public Vector2Int PlacementSize { get; private set; }

			public Vector2Int Position { get; private set; }

			public StructureAsset Structure { get; private set; }

			public int Surrounds { get; set; }

			public Leaf()
			{
				Density = new DensitySpatial() { Leaf = this };
				Placement = new PlacementSpatial() { Leaf = this };
			}

			public class DensitySpatial : ISpatial
			{
				public Leaf Leaf { get; set; }

				int ISpatial.CenterX => Leaf.Position.x;

				int ISpatial.CenterY => Leaf.Position.y;

				int ISpatial.Height => Leaf.DensityRadius;

				int ISpatial.Width => Leaf.DensityRadius;

				bool IEquatable<ISpatial>.Equals(ISpatial other)
				{
					if (!(other is DensitySpatial))
					{
						return false;
					}

					var otherSpatial = (DensitySpatial)other;
					return otherSpatial.Leaf == Leaf;
				}
			}

			public class LeafBuilder : IStructurePlacement
			{
				private int density;
				private Vector2Int position;
				private Vector2Int size;
				private StructureAsset structure;

				public Leaf Build()
				{
					return new Leaf
					{
						DensityRadius = density,
						PlacementSize = size,
						Position = position,
						Structure = structure
					};
				}

				public IStructurePlacement WithBiomeStructureDefinition(ref BiomeStructureDefinition structureDefinition)
				{
					density = structureDefinition.densityRadius;
					structure = structureDefinition.structure;
					return this;
				}

				public IStructurePlacement WithDensity(int density)
				{
					this.density = density;
					return this;
				}

				public IStructurePlacement WithPosition(Vector2Int position)
				{
					this.position = position;
					return this;
				}

				public IStructurePlacement WithSize(Vector2Int size)
				{
					this.size = size;
					return this;
				}

				public IStructurePlacement WithStructure(StructureAsset structure)
				{
					this.structure = structure;
					return this;
				}
			}

			public class PlacementSpatial : ISpatial
			{
				public Leaf Leaf { get; set; }

				int ISpatial.CenterX => Leaf.Position.x;

				int ISpatial.CenterY => Leaf.Position.y;

				int ISpatial.Height => Leaf.PlacementSize.y;

				int ISpatial.Width => Leaf.PlacementSize.x;

				bool IEquatable<ISpatial>.Equals(ISpatial other)
				{
					if (!(other is PlacementSpatial))
					{
						return false;
					}

					var otherSpatial = (PlacementSpatial)other;
					return otherSpatial.Leaf == Leaf;
				}
			}
		}

		private class StructureGeneratorContext : IStructureGeneratorContext
		{
			private readonly Vector2Int chunkPosition;
			private readonly int chunkSize;
			private readonly List<Leaf> leaves;
			private readonly Queue<Leaf> postProcessing;
			private readonly Random random;
			private readonly IStructureContainer structureContainer;
			private readonly StructureGenerator structureGenerator;
			private readonly Func<Vector2Int, ITile> tileAccessor;

			public StructureGeneratorContext(StructureGenerator structureGenerator, Random random, Vector2Int chunkPosition, int chunkSize, IStructureContainer structureContainer, Func<Vector2Int, ITile> tileAccessor)
			{
				this.structureGenerator = structureGenerator;
				this.random = random;
				this.chunkPosition = chunkPosition * chunkSize;
				this.chunkSize = chunkSize;
				this.structureContainer = structureContainer;
				this.tileAccessor = tileAccessor;

				leaves = new List<Leaf>();
				postProcessing = new Queue<Leaf>();
			}

			public void Annotate()
			{
				foreach (var leaf in leaves)
				{
					leaf.Surrounds = leaves.Count(x => x != leaf && x.Structure != leaf.Structure && Mathf.Max(Mathf.Abs(x.Position.x - leaf.Position.x), Mathf.Abs(x.Position.y - leaf.Position.y)) < leaf.DensityRadius);
					leaf.Overlapping = leaves.Count(x => x != leaf && Mathf.Abs(x.Position.x - leaf.Position.x) < (leaf.PlacementSize.x + x.PlacementSize.x) / 2 && Mathf.Abs(x.Position.y - leaf.Position.y) < (leaf.PlacementSize.y + x.PlacementSize.y) / 2);
				}
			}

			public void CreatePlacement()
			{
				var length = chunkSize * chunkSize;
				for (int i = 0; i < length; i++)
				{
					var v = new Vector2Int(i % chunkSize, i / chunkSize % chunkSize);

					var tile = tileAccessor(v);
					var biome = tile.Biome;

					var structureDefinitions = Array.FindAll(biome.StructureDefinitions ?? Array.Empty<BiomeStructureDefinition>(), s => s.chance < random.NextDouble() && s.structure.CanSpawn(tile, v, this));
					if (structureDefinitions.Length == 0)
					{
						continue;
					}

					var probability = Enumerable.Range(0, structureDefinitions.Length).Select(x => 1 - random.NextDouble()).ToArray();
					Array.Sort(probability, structureDefinitions);
					var builder = new Leaf.LeafBuilder();
					builder.WithPosition(v);
					structureDefinitions[0].Place(builder);

					var leaf = builder.Build();
					leaves.Add(leaf);
					postProcessing.Enqueue(leaf);
				}
			}

			public void Filter()
			{
				FilterOverlapping();
				FilterDensity();
				for (int i = 0, n = leaves.Count; i < n; i++)
				{
					postProcessing.Enqueue(leaves[i]);
				}
			}

			public void PostProcess()
			{
				while (postProcessing.Count > 0)
				{
					var leaf = postProcessing.Dequeue();
					leaf.Structure.PostProcess();
				}
			}

			public void Spawn()
			{
				foreach (var item in leaves)
				{
					var spawn = structureContainer.Spawn();
					spawn.WithPosition(item.Position);
					item.Structure.Spawn(spawn);
					spawn.Spawn();
				}
			}

			private void FilterDensity()
			{
				var orderedLeaves = leaves.Where(x => x.Surrounds > 0).OrderBy(x => x.Surrounds);
				bool noneFiltered = false;
				while (!noneFiltered)
				{
					noneFiltered = true;

					foreach (var item in orderedLeaves)
					{
						Leaf filter = null;
						var nearestDistance = float.PositiveInfinity;
						foreach (var spatial in leaves)
						{
							if (spatial == item)
							{
								continue;
							}
							if (spatial.Structure != item.Structure)
							{
								continue;
							}

							var dx = Mathf.Abs(item.Position.x - spatial.Position.x);
							var dy = Mathf.Abs(item.Position.y - spatial.Position.y);

							var d = dx * dx + dy * dy;
							if (Mathf.Max(dx, dy) <= item.DensityRadius && d < nearestDistance)
							{
								nearestDistance = d;
								filter = spatial;
							}
						}
						if (filter != null)
						{
							item.Surrounds -= 1;
							noneFiltered = false;
							leaves.Remove(filter);
							break;
						}
					}
				}
			}

			private void FilterOverlapping()
			{
				var orderedLeaves = leaves.Where(x => x.Overlapping > 0).OrderBy(x => x.Overlapping);
				bool noneFiltered = false;
				while (!noneFiltered)
				{
					noneFiltered = true;

					foreach (var item in orderedLeaves)
					{
						Leaf filter = null;
						var nearestDistance = float.PositiveInfinity;
						foreach (var spatial in leaves)
						{
							if (spatial == item)
							{
								continue;
							}

							var dx = Mathf.Abs(item.Position.x - spatial.Position.x);
							var dy = Mathf.Abs(item.Position.y - spatial.Position.y);
							var sx = (item.PlacementSize.x + spatial.PlacementSize.x) / 2;
							var sy = (item.PlacementSize.y + spatial.PlacementSize.y) / 2;

							var d = dx * dx + dy * dy;
							if (dx < sx && dy < sy && d < nearestDistance)
							{
								nearestDistance = d;
								filter = spatial;
							}
						}
						if (filter != null)
						{
							item.Overlapping -= 1;
							noneFiltered = false;
							leaves.Remove(filter);
							break;
						}
					}
				}
			}
		}
	}
}
