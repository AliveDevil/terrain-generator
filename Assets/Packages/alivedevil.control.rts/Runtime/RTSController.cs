using Cinemachine;
using System;
using TerrainGenerator;
using TerrainGenerator.Core;
using UnityEngine;

public class RTSController : InputController
{
	[SerializeField]
	private CinemachineVirtualCamera controlledCamera = null;
	[SerializeField]
	private CurveSet curves;
	private Vector3 movement;
	[SerializeField]
	private float movementSpeed = 15;
	private float pitch;
	private Rotation rotation;
	private TerrainState terrainState;
	[SerializeField]
	private float zoom = 0.5f;

	public override CinemachineVirtualCamera ControlledCamera => controlledCamera;

	public float Zoom
	{
		get { return zoom; }
		set { zoom = Mathf.Clamp01(value); }
	}

	public void Move(float x, float y) => Move(new Vector2(x, y));

	public void Move(Vector2 movement)
	{
		this.movement += Quaternion.Euler(0, rotation.Yaw, 0) * new Vector3(movement.x, 0, movement.y) * curves.Movement.Evaluate(zoom);
	}

	public void Pitch(float delta)
	{
		rotation.Pitch += delta * curves.Rotation.Evaluate(zoom);
		pitch = (rotation.Pitch - curves.MinPitch.Evaluate(zoom)) / (curves.MaxPitch.Evaluate(zoom) - curves.MinPitch.Evaluate(zoom));
	}

	public void Yaw(float delta)
	{
		rotation.Yaw += delta * curves.Rotation.Evaluate(zoom);
	}

	protected override void OnCameraUpdate()
	{
		var movement = this.movement;
		this.movement = Vector3.zero;

		var position = transform.position;
		position += movement * Time.deltaTime;
		var v2 = new Vector2Int(Mathf.RoundToInt(position.x), Mathf.RoundToInt(position.z));
		var sample = terrainState.TerrainController.Sample(v2);
		float targetHeight = curves.CameraHeight.Evaluate(zoom) + sample.GetHeight(false); // TODO GameState gameState.TerrainState.TerrainController.GetHeight(v2);
		position.y = targetHeight; // Mathf.MoveTowards(position.y, targetHeight, Time.deltaTime * 20);

		transform.position = position;

		pitch += (Mathf.Clamp01(pitch) - pitch) * Time.deltaTime;
		rotation.Pitch = (1 - pitch) * curves.MinPitch.Evaluate(zoom) + pitch * curves.MaxPitch.Evaluate(zoom);
		transform.localRotation = rotation.Quaternion;

		// TODO gameState.PlayerFollow.position = transform.position;
	}

	protected override void OnCheckInput()
	{
		Vector2 direction = Vector2.zero;
		direction.x = Input.GetAxis("Horizontal");
		direction.y = Input.GetAxis("Vertical");

		if (direction != Vector2.zero)
		{
			direction.Normalize();
			Move(direction * movementSpeed);
		}
	}

	protected override void OnEnable()
	{
		base.OnEnable();
		terrainState = TerrainState.PerScene(gameObject);
	}

	protected override void OnMovement()
	{
	}

	protected override void OnSwitchFrom()
	{
	}

	protected override void OnSwitchTo()
	{
		// TODO transform.position = gameState.PlayerFollow.position;
	}

	[Serializable]
	private struct CurveSet
	{
		public AnimationCurve CameraHeight;
		public AnimationCurve MaxPitch;
		public AnimationCurve MinPitch;
		public AnimationCurve Movement;
		public AnimationCurve Rotation;
	}
}
