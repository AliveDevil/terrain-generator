using UnityEngine;
using UnityEngine.EventSystems;

public class RTSMouseControl : MonoBehaviour, IScrollHandler, IDragHandler, IPointerEnterHandler, IUpdateSelectedHandler
{
	private InputState inputState;
	private RTSController rtsController;

	void IDragHandler.OnDrag(PointerEventData eventData)
	{
		switch (eventData.button)
		{
			case PointerEventData.InputButton.Left:
				eventData.selectedObject = gameObject;

				rtsController.Move(-eventData.delta);
				break;

			case PointerEventData.InputButton.Right:
				eventData.selectedObject = gameObject;

				rtsController.Yaw(-eventData.delta.x * Time.deltaTime);
				rtsController.Pitch(eventData.delta.y * Time.deltaTime);
				break;

			case PointerEventData.InputButton.Middle:
				break;

			default:
				break;
		}
	}

	void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData)
	{
		EventSystem.current.SetSelectedGameObject(gameObject, eventData);
	}

	void IScrollHandler.OnScroll(PointerEventData eventData)
	{
		rtsController.Zoom += eventData.scrollDelta.y * Time.deltaTime;
	}

	void IUpdateSelectedHandler.OnUpdateSelected(BaseEventData eventData)
	{
	}

	private void OnEnable()
	{
		inputState = InputState.Active;
		rtsController = (RTSController)inputState.InputController[0];
	}
}
