using TerrainGenerator.Data.Assets;
using TerrainGenerator.Extensions;
using TerrainGenerator.ThirdParty.OpenSimplexNoise;
using UnityEngine;

namespace TerrainGenerator.Data
{
	public class NoiseObject
	{
		private readonly bool abs;
		private readonly float amplitude;
		private readonly OpenSimplexNoise noise;
		private readonly int octaves;
		private readonly float persistence;
		private readonly float scale;

		public NoiseObject(Config config)
		{
			abs = config.Abs;
			amplitude = config.Amplitude;
			octaves = config.Octaves;
			persistence = config.Persistence;
			scale = config.Scale;
			noise = new OpenSimplexNoise(config.Seed.GetHashCode());
		}

		public float Evaluate(Vector2 v)
		{
			return Evaluate(noise.EvaluateOctave(v, 1 / scale, persistence, octaves), abs) * amplitude;
		}

		public float Evaluate(Vector3 v)
		{
			return Evaluate(noise.EvaluateOctave(v, 1 / scale, persistence, octaves), abs) * amplitude;
		}

		public float Evaluate(float x, float y)
		{
			return Evaluate(noise.EvaluateOctave(x, y, 1 / scale, persistence, octaves), abs) * amplitude;
		}

		public float Evaluate(float x, float y, float z)
		{
			return Evaluate(noise.EvaluateOctave(x, y, z, 1 / scale, persistence, octaves), abs) * amplitude;
		}

		private static float Evaluate(float v, bool abs)
		{
			if (abs)
			{
				return Mathf.Abs(v);
			}
			return v;
		}

		public struct Config
		{
			public static readonly Config Default = new Config(string.Empty, false, 1, 1, 1, 1);
			public readonly bool Abs;
			public readonly float Amplitude;
			public readonly int Octaves;
			public readonly float Persistence;
			public readonly float Scale;
			public readonly string Seed;

			public Config(string seed, bool abs, float amplitude, int octaves, float persistence, float scale)
			{
				Seed = seed;
				Abs = abs;
				Amplitude = amplitude;
				Octaves = octaves;
				Persistence = persistence;
				Scale = scale;
			}

			public static Config GetConfig(string seed, NoiseAsset noise)
			{
				return new Config(seed + noise.name, noise.Absolute, 1, noise.Octaves, noise.Persistence, 1);
			}

			public static Config GetConfig(string seed, NoiseAsset noise, NoiseLayerAsset layer)
			{
				return new Config(seed + noise.name + layer.name, noise.Absolute, layer.Persistence, noise.Octaves, noise.Persistence, layer.Scale);
			}

			public static Config GetConfig(string seed, NoiseAsset noise, NoiseLayerAsset layer, float scale)
			{
				return new Config(seed + noise.name + layer.name, noise.Absolute, layer.Persistence, noise.Octaves, noise.Persistence, layer.Scale * scale);
			}
		}
	}
}
