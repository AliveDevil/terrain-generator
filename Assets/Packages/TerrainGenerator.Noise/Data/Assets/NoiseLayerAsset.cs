using UnityEngine;

namespace TerrainGenerator.Data.Assets
{
	public class NoiseLayerAsset : ScriptableObject
	{
		[SerializeField]
		private float persistence = 1;
		[SerializeField]
		private int scale = 1;

		public float Persistence
		{
			get
			{
				return persistence;
			}
		}

		public int Scale
		{
			get
			{
				return scale;
			}
		}
	}
}
