using UnityEngine;

namespace TerrainGenerator.Data.Assets
{
	public class NoiseAsset : ScriptableObject
	{
		[SerializeField]
		private bool absolute;
		[SerializeField]
		private int octaves = 3;
		[SerializeField]
		private float persistence = 1;

		public bool Absolute
		{
			get { return absolute; }
		}

		public int Octaves
		{
			get { return octaves; }
		}

		public float Persistence
		{
			get { return persistence; }
		}
	}
}
