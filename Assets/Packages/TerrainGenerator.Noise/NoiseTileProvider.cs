using System;
using TerrainGenerator.Data;
using TerrainGenerator.Data.Assets;
using UnityEngine;

namespace TerrainGenerator.Terrain
{
	public class NoiseTileProvider : TileProvider
	{
		[SerializeField]
		private Config config;
		private Runtime runtime;

		protected override void OnElevate(TerrainSample sample)
		{
			if (sample.Surface)
			{
				var value = runtime.ClassNoise.Evaluate(sample.Position);
				sample.Elevation = GetHeight(sample.Position, Mathf.Abs(value), sample.Biome.TerrainAmplitude);
			}
			else
			{
				sample.Elevation = 0;
			}
		}

		protected override void OnInitializeTileProvider()
		{
			InitializeNoise("");
		}

		protected override void OnRegisterTileProvider()
		{
		}

		protected override void OnSample(TerrainSample sample)
		{
			var value = runtime.ClassNoise.Evaluate(sample.Position);
			sample.Surface = value >= 0;
			sample.Temperature = GetTemperature(sample.Position);
			sample.Rainfall = GetRainfall(sample.Position);
		}

		protected override void OnSetupTileProvider()
		{
		}

		private void CreateNoise(string seed, NoiseAsset noise, out NoiseObject local, out NoiseObject global)
		{
			local = new NoiseObject(NoiseObject.Config.GetConfig(seed, noise, config.LocalNoiseLayer, TerrainState.SizeFactor));
			global = new NoiseObject(NoiseObject.Config.GetConfig(seed, noise, config.GlobalNoiseLayer, TerrainState.SizeFactor));
		}

		private float Evaluate(Vector2Int v, NoiseGroup group)
		{
			return (group.Global.Evaluate(v) + group.Local.Evaluate(v)) / config.Persistence;
		}

		private float EvaluateAbs(Vector2Int v, NoiseGroup group)
		{
			return (Mathf.Abs(group.Global.Evaluate(v)) + Mathf.Abs(group.Local.Evaluate(v))) / config.Persistence;
		}

		private int GetHeight(Vector2Int world, float value, int amplitude)
		{
			float height = Evaluate(world, runtime.Height) * value;
			float globalHeight = Mathf.Abs(height) * config.Amplitude;
			float biomeHeight = height * amplitude;

			return Mathf.FloorToInt(globalHeight + biomeHeight);
		}

		private float GetRainfall(Vector2Int world)
		{
			return Evaluate(world, runtime.Rainfall);
		}

		private float GetTemperature(Vector2Int world)
		{
			return Evaluate(world, runtime.Temperature);
		}

		private void InitializeClassNoise(string seed)
		{
			runtime.ClassNoise = new NoiseObject(NoiseObject.Config.GetConfig(seed, config.ClassNoise, config.ClassNoiseLayer, TerrainState.SizeFactor));
		}

		private void InitializeHeightNoise(string seed)
		{
			var local = default(NoiseObject);
			var global = default(NoiseObject);
			CreateNoise(seed, config.HeightNoise, out local, out global);
			runtime.Local.Height = local;
			runtime.Global.Height = global;
			runtime.Height.Local = local;
			runtime.Height.Global = global;
		}

		private void InitializeHumidityNoise(string seed)
		{
			var local = default(NoiseObject);
			var global = default(NoiseObject);
			CreateNoise(seed, config.HumidityNoise, out local, out global);
			runtime.Local.Humidity = local;
			runtime.Global.Humidity = global;
			runtime.Humidity.Local = local;
			runtime.Humidity.Global = global;
		}

		private void InitializeNoise(string seed)
		{
			InitializeClassNoise(seed);
			InitializeHeightNoise(seed);
			InitializeHumidityNoise(seed);
			InitializeRainfallNoise(seed);
			InitializeTemperatureNoise(seed);
		}

		private void InitializeRainfallNoise(string seed)
		{
			var local = default(NoiseObject);
			var global = default(NoiseObject);
			CreateNoise(seed, config.RainfallNoise, out local, out global);
			runtime.Local.Rainfall = local;
			runtime.Global.Rainfall = global;
			runtime.Rainfall.Local = local;
			runtime.Rainfall.Global = global;
		}

		private void InitializeTemperatureNoise(string seed)
		{
			var local = default(NoiseObject);
			var global = default(NoiseObject);
			CreateNoise(seed, config.TemperatureNoise, out local, out global);
			runtime.Local.Temperature = local;
			runtime.Global.Temperature = global;
			runtime.Temperature.Local = local;
			runtime.Temperature.Global = global;
		}

		[Serializable]
		private struct Config
		{
			public int Amplitude;
			public NoiseAsset ClassNoise;
			public NoiseLayerAsset ClassNoiseLayer;
			public NoiseLayerAsset GlobalNoiseLayer;
			public NoiseAsset HeightNoise;
			public NoiseAsset HumidityNoise;
			public NoiseLayerAsset LocalNoiseLayer;
			public NoiseAsset RainfallNoise;
			public NoiseAsset TemperatureNoise;

			public float Persistence => GlobalNoiseLayer.Persistence + LocalNoiseLayer.Persistence;
		}

		private struct NoiseGroup
		{
			public NoiseObject Global;
			public NoiseObject Local;
		}

		private struct PartialRuntime
		{
			public NoiseObject Height;
			public NoiseObject Humidity;
			public NoiseObject Rainfall;
			public NoiseObject Temperature;
		}

		private struct Runtime
		{
			public NoiseObject ClassNoise;
			public PartialRuntime Global;
			public NoiseGroup Height;
			public NoiseGroup Humidity;
			public PartialRuntime Local;
			public NoiseGroup Rainfall;
			public NoiseGroup Temperature;
		}
	}
}
