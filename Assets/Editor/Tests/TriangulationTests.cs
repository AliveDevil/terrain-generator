using d3.delaunay;
using NUnit.Framework;
using redblobgames;
using System;
using TerrainGenerator.Extensions;
using UnityEngine;
using Random = System.Random;

namespace TerrainGenerator.Tests
{
	[TestFixture]
	public class TriangulationTests
	{
		private const int C = 106;
		private const int HALFHEIGHT = HEIGHT / 2;
		private const int HALFWIDTH = WIDTH / 2;
		private const int HEIGHT = 12;
		private const int HEIGHTINC = HEIGHT + 1;
		private const int WIDTH = 20;
		private const int WIDTHINC = WIDTH + 1;
		private Delaunay delaunay;
		private DualMesh dualMesh;
		private Voronoi voronoi;

		public static Vector3? IsPointInTriangle(Vector2 p, Vector2 p1, Vector2 p2, Vector2 p3)
		{
			//Based on Barycentric coordinates
			float denominator = ((p2.y - p3.y) * (p1.x - p3.x) + (p3.x - p2.x) * (p1.y - p3.y));

			float a = ((p2.y - p3.y) * (p.x - p3.x) + (p3.x - p2.x) * (p.y - p3.y)) / denominator;
			if (Mathf.Abs(a) <= 1E-7)
			{
				a = 0;
			}
			float b = ((p3.y - p1.y) * (p.x - p3.x) + (p1.x - p3.x) * (p.y - p3.y)) / denominator;
			if (Mathf.Abs(b) <= 1E-7)
			{
				b = 0;
			}
			float c = 1 - a - b;
			if (Mathf.Abs(c) <= 1E-7)
			{
				c = 0;
			}

			//The point is within the triangle or on the border if 0 <= a <= 1 and 0 <= b <= 1 and 0 <= c <= 1
			if (a >= 0f && a <= 1f && b >= 0f && b <= 1f && c >= 0f && c <= 1f)
			{
				return new Vector3(a, (float)b, (float)c);
			}

			return null;
		}

		[SetUp]
		public void Setup()
		{
			var random = new Random(0);
			var surfaceRandom = new Random(random.Next());

			var width = WIDTH;
			var height = HEIGHT;
			var xMin = -HALFWIDTH;
			var yMin = -HALFHEIGHT;
			var xMax = HALFWIDTH;
			var yMax = HALFHEIGHT;

			int c = C;
			var points = new Vector2[c + 4];
			for (int i = 0; i < c; i++)
			{
				Vector2Int v = Vector2Int.zero;
				bool found = false;
				while (!found)
				{
					v.x = RangedInt(surfaceRandom, xMin, xMax);
					v.y = RangedInt(surfaceRandom, yMin, yMax);

					found = true;
					for (int p = i - 1; p >= 0; p--)
					{
						if (points[p] == v)
						{
							found = false;
						}
					}
				}
				points[i] = v;
			}

			points[c + 0] = new Vector2(xMin - xMax, yMin - yMax);
			points[c + 1] = new Vector2(xMin - xMax, yMax + yMax);
			points[c + 2] = new Vector2(xMax + xMax, yMax + yMax);
			points[c + 3] = new Vector2(xMax + xMax, yMin - yMax);

			delaunay = new Delaunay(points);
			voronoi = delaunay.Voronoi(new Rect(xMin, yMin, width, height));
			dualMesh = delaunay.DualMesh();
		}

		[Test]
		public void TestFindRegion()
		{
			Loop((x, y, p) =>
			{
				var region = voronoi.Find(p);
				Assert.Less(region, C);
			});
		}

		[Test]
		public void TestTriangulation()
		{
			Loop((x, y, p) =>
			{
				var region = voronoi.Find(p);
				var regionPoint = delaunay.Points[region];

				if (p == regionPoint)
				{
					return;
				}

				var v = voronoi.CellRaw(region);
				for (int i = 0; i < v.Length; i++)
				{
					var c = v[i];
					var pC = voronoi.Circumcenters[c];
					if (pC == p)
					{
						return;
					}
				}

				for (int i = 0, n = v.Length, v0 = 0, v1 = v[n - 1]; i < n; i++)
				{
					v0 = v1;
					v1 = v[i];

					var p0 = voronoi.Circumcenters[v0];
					var p1 = voronoi.Circumcenters[v1];

					if (x == -6 && y == -1 && i == n - 1)
					{
					}

					if (TriangleExtensions.accuratePointInTriangle(p0, p1, regionPoint, p))
					{
						return;
					}
				}

				Assert.Fail($"Did not find triangle for {p}.");
			});
		}

		private static void Loop(Action<int, int, Vector2> action)
		{
			var xMin = -HALFWIDTH;
			var yMin = -HALFHEIGHT;
			var xMax = HALFWIDTH;
			var yMax = HALFHEIGHT;

			for (int x = xMin; x <= xMax; x++)
			{
				for (int y = yMin; y <= yMax; y++)
				{
					var p = new Vector2(x, y);
					action(x, y, p);
				}
			}
		}

		private static int RangedInt(Random random, int minimum, int maximum)
		{
			return Mathf.RoundToInt((float)random.NextDouble() * 8 * (maximum - minimum)) / 8 + minimum;
		}
	}
}
