using UnityEditor;
using UnityEditor.ProjectWindowCallback;

namespace TerrainGenerator.Actions
{
	public class EndNameEdit : EndNameEditAction
	{
		public override void Action(int instanceId, string pathName, string resourceFile)
		{
			AssetDatabase.CreateAsset(EditorUtility.InstanceIDToObject(instanceId), AssetDatabase.GenerateUniqueAssetPath(pathName));
		}
	}
}
